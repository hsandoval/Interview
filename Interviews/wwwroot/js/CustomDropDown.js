function PopulateComboBox(idTagHtml, nameDataContainer = 'childrenTypes') {
    const tagHtml = $(`#${idTagHtml}`);

    let customTags = tagHtml.data('options');
    const urlRequest = `${customTags.Controller}/${customTags.Method}`;

    let obj = {};
    let HttpRequest = MakeRequest(urlRequest, obj);

    let idModal = customTags.IdModal;

    HttpRequest
        .then(res => {
            ResetValues(idTagHtml);
            const dataSelect = ParseData($.parseJSON(res.body), nameDataContainer, customTags);
            if (IsNullOrEmpty(idModal)) {
                tagHtml.select2({
                    placeholder: 'Seleccione una opción',
                    data: dataSelect,
                    theme: 'select2 select2-container select2-container--bootstrap'
                });
            } else {
                tagHtml.select2({
                    placeholder: 'Seleccione una opción',
                    data: dataSelect,
                    theme: 'select2 select2-container select2-container--bootstrap',
                    dropdownParent: $(`#${idModal}`)
                });
            }
        }).catch((err) => {
            console.log(`Algo salió mal: ${err}`);
        });
};

function PopulateComboBoxWithData(idTagHtml, data, nameDataContainer = 'childrenTypes') {
    if ($.parseJSON(data).length < 1) {
        ResetValues(idTagHtml);
        return
    }
    const tagHtml = $(`#${idTagHtml}`);

    let customTags = tagHtml.data('options');
    let idModal = customTags.IdModal;

    const dataSelect = ParseData($.parseJSON(data), nameDataContainer, customTags);
    ResetValues(idTagHtml);
    if (IsNullOrEmpty(idModal)) {
        tagHtml.select2({
            placeholder: 'Seleccione una opción',
            data: dataSelect,
            theme: 'select2 select2-container select2-container--bootstrap'
        });
    } else {
        tagHtml.select2({
            placeholder: 'Seleccione una opción',
            data: dataSelect,
            theme: 'select2 select2-container select2-container--bootstrap',
            dropdownParent: $(`#${idModal}`)
        });
    }

};

function ParseData(rawData, nameDataContainer, tags) {
    const IdSelected = tags.Selected;
    const IdField = tags.IdField;
    const DescriptionField = tags.DescriptionField;
    let dataProcessed;
    if (rawData[nameDataContainer] === undefined) {
        dataProcessed = $.map(rawData, function (obj) {
            let data = _.allKeys(obj);

            obj.text = obj[DescriptionField];
            obj.id = obj[IdField];
            if (obj.id === IdSelected) obj.selected = true;

            return obj;
        });
    } else {
        dataProcessed = $.map(rawData[nameDataContainer], function (obj) {
            let data = _.allKeys(obj);

            obj.text = obj[DescriptionField];
            obj.id = obj[IdField];
            if (obj.id === IdSelected) obj.selected = true;

            return obj;
        });
    }
    return dataProcessed;
}

function ResetValues(idTagHtml) {
    const tagHtml = $(`#${idTagHtml}`);
    tagHtml.html('').select2({
        data: [{ id: '', text: '' }],
        placeholder: 'No existen registros coincidentes',
        theme: 'select2 select2-container select2-container--bootstrap'
    });
}

function PopulateComboBoxFontAwesome(idTagHtml, rawData) {
    const tagHtml = $(`#${idTagHtml}`);

    let customTags = tagHtml.data('options');
    let idModal = customTags.IdModal;

    $(`#${idTagHtml}`).select2({
        placeholder: 'Seleccione una opción',
        data: rawData,
        theme: 'select2 select2-container select2-container--bootstrap',
        dropdownParent: $(`#${idModal}`)
    });
}

function formatState(state) {
    if (!state.id) { return state.text; }
    var $state = $(`<span class="${state.text}"/>${state.text}</span>'`);
    return $state;
}