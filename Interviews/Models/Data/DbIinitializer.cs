using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Identity;

namespace Interviews.Models.Data
{
    public class DbInitializer
    {
        private readonly ApplicationContext Context;
        private readonly UserManager<UserApplication> UserManager;
        private readonly RoleManager<IdentityRole> RoleManager;

        public DbInitializer(ApplicationContext context, UserManager<UserApplication> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.Context = context;
            this.UserManager = userManager;
            this.RoleManager = roleManager;
        }

        public async Task Initialize()
        {
            Context.Database.EnsureCreated();

            FillDataIconsFontAwesome();
            FillDataCategories();
            FillDataPersonalInformationRequired();
            FillDataPersons();
            await FillRoles();
            await GenerateUsers();
        }

        private void FillDataIconsFontAwesome()
        {
            if (Context.IconsFontAwesome.Any())
                return;

            List<IconFontAwesome> list = new List<IconFontAwesome>
            {
                new IconFontAwesome { TX_Description = "fa fa-coins" }
            };

            Context.IconsFontAwesome.AddRange(list);
            Context.SaveChanges();
        }

        private void FillDataCategories()
        {
            if (Context.Categories.Any())
                return;

            List<Category> categories = new List<Category> {
                new Category { TX_Description = "Pais", BO_IsTypeFather = false, FK_IconFontAwesome = 1 },
                new Category { TX_Description = "Estado", BO_IsTypeFather = false, FK_IconFontAwesome = 1 },
                new Category { TX_Description = "Ciudad", BO_IsTypeFather = false, FK_IconFontAwesome = 1 },
                new Category { TX_Description = "Sector", BO_IsTypeFather = false, FK_IconFontAwesome = 1 },
                new Category { TX_Description = "Departamento", BO_IsTypeFather = false, FK_IconFontAwesome = 1 },
                new Category { TX_Description = "Corregimiento", BO_IsTypeFather = false, FK_IconFontAwesome = 1 },
                new Category { TX_Description = "Tópicos", BO_IsTypeFather = false, FK_IconFontAwesome = 1 },
                new Category {
                    TX_Description = "Localidades",
                    BO_IsTypeFather = true,
                    FK_IconFontAwesome = 1,
                    Types = new List<Entities.Type> {
                        new Entities.Type {
                            TX_Description = "Venezuela",
                            FK_Category = 1, FK_IconFontAwesome = 1,
                            ChildrenTypes = new List<Entities.Type>{
                                new Entities.Type {
                                    TX_Description = "Caracas",
                                    FK_Category = 2,
                                    FK_IconFontAwesome = 1,
                                    ChildrenTypes = new List<Entities.Type>{
                                        new Entities.Type { TX_Description = "Altamira", FK_Category = 4, FK_IconFontAwesome = 1 },
                                        new Entities.Type { TX_Description = "Chacao", FK_Category = 4, FK_IconFontAwesome = 1 },
                                        new Entities.Type { TX_Description = "Propatria", FK_Category = 4, FK_IconFontAwesome = 1 },
                                        new Entities.Type { TX_Description = "Plaza Sucre", FK_Category = 4, FK_IconFontAwesome = 1 },
                                        new Entities.Type { TX_Description = "Perez Bonald", FK_Category = 4, FK_IconFontAwesome = 1 },
                                        new Entities.Type { TX_Description = "Chacaito", FK_Category = 4, FK_IconFontAwesome = 1 },
                                        new Entities.Type { TX_Description = "Capitolio", FK_Category = 4, FK_IconFontAwesome = 1 },
                                        new Entities.Type { TX_Description = "Parque Carabobo", FK_Category = 4, FK_IconFontAwesome = 1 },
                                    }
                                },
                                new Entities.Type { TX_Description = "Miranda", FK_Category = 2, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Vargas", FK_Category = 2, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Aragua", FK_Category = 2, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Maracaibo", FK_Category = 2, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Zulia", FK_Category = 2, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Caracabobo", FK_Category = 2, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Barquisimeto", FK_Category = 2, FK_IconFontAwesome = 1 },
                            }
                        },
                        new Entities.Type {
                            TX_Description = "Colombia",
                            FK_Category = 1,
                            FK_IconFontAwesome = 1,
                            ChildrenTypes = new List<Entities.Type>{
                                new Entities.Type { TX_Description = "Amazonas", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Antioquia", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Santander", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Puerto Alegría", FK_Category = 7, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "La Chochera", FK_Category = 7, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Bogotá", FK_Category = 7, FK_IconFontAwesome = 1 },
                            }
                        },
                    },
                },
                new Category {
                    TX_Description = "Géneros",
                    BO_IsTypeFather = true,
                    FK_IconFontAwesome = 1,
                    Types = new List<Entities.Type>
                    {
                        new Entities.Type { TX_Description = "Femenino", FK_IconFontAwesome = 1 },
                        new Entities.Type { TX_Description = "Masculino", FK_IconFontAwesome = 1 },
                    }
                },
                new Category {
                    TX_Description = "Documentos identificación",
                    BO_IsTypeFather = true,
                    FK_IconFontAwesome = 1,
                    Types = new List<Entities.Type>
                    {
                        new Entities.Type { TX_Description = "Cédula identidad", FK_Category = 8, FK_IconFontAwesome = 1 },
                        new Entities.Type { TX_Description = "Pasaporte", FK_Category = 8, FK_IconFontAwesome = 1 },
                        new Entities.Type { TX_Description = "Carnet de la patria", FK_Category = 8, FK_IconFontAwesome = 1 },
                    }
                },
                new Category {
                    TX_Description = "Redes sociales",
                    BO_IsTypeFather = true,
                    FK_IconFontAwesome = 1,
                    Types = new List<Entities.Type>
                    {
                        new Entities.Type { TX_Description = "Facebook", FK_IconFontAwesome = 1 },
                        new Entities.Type { TX_Description = "Twitter", FK_IconFontAwesome = 1 },
                        new Entities.Type { TX_Description = "Instagram", FK_IconFontAwesome = 1 },
                    }
                },
                
                new Category {
                    TX_Description = "Estatus entrevistas",
                    BO_IsTypeFather = true,
                    FK_IconFontAwesome = 1,
                    Types = new List<Entities.Type>
                    {
                        new Entities.Type { TX_Description = "Pendiente por respuesta", FK_IconFontAwesome = 1 },
                        new Entities.Type { TX_Description = "Aceptada", FK_IconFontAwesome = 1 },
                        new Entities.Type { TX_Description = "Procesada", FK_IconFontAwesome = 1 },
                    }
                },
                new Category {
                    TX_Description = "Materias",
                    BO_IsTypeFather = true,
                    FK_IconFontAwesome = 1,
                    Types = new List<Entities.Type>
                    {
                        new Entities.Type {
                            TX_Description = "Programación",
                            FK_IconFontAwesome = 1,
                            ChildrenTypes = new List<Entities.Type>
                            {
                                new Entities.Type { TX_Description = "PHP", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "C#", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Java", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Haskell", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Base de datos", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Javascript", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "C++", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Python", FK_Category = 6, FK_IconFontAwesome = 1 },
                            }
                        },
                        new Entities.Type {
                            TX_Description = "Administración",
                            FK_IconFontAwesome = 1,
                            ChildrenTypes = new List<Entities.Type>{
                                new Entities.Type { TX_Description = "Administración Financiera", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Gerencia", FK_Category = 6, FK_IconFontAwesome = 1 },
                                new Entities.Type { TX_Description = "Contabilidad", FK_Category = 6, FK_IconFontAwesome = 1 },
                            }
                        }
                    }
                }
            };

            Context.Categories.AddRange(categories);
            Context.SaveChanges();
        }

        private void FillDataPersonalInformationRequired()
        {
            if (Context.PersonalInformationRequired.Any())
                return;

            List<PersonalInformationRequired> personalInformationRequired = new List<PersonalInformationRequired> {
                new PersonalInformationRequired { BO_IsDropDownList = false, TX_Description = "Dirección habitación", FK_AssociatedCategory = 1 },
                new PersonalInformationRequired { BO_IsDropDownList = false, TX_Description = "Dirección trabajo", FK_AssociatedCategory = 1 },
                new PersonalInformationRequired { BO_IsDropDownList = true, TX_Description = "Identificación", FK_AssociatedCategory = 9 },
                new PersonalInformationRequired { BO_IsDropDownList = true, TX_Description = "Redes sociales", FK_AssociatedCategory = 1 },
            };

            personalInformationRequired.ForEach(x => Context.PersonalInformationRequired.Add(x));

            Context.SaveChanges();
        }

        private void FillDataPersons()
        {
            if (Context.Persons.Any())
            {
                return;
            }

            List<Professor> professors = new List<Professor>
            {
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 2,
                        TX_FirtsName = "David",
                        TX_LastName = "Hernandez",
                        DT_Birthday = new DateTime(1978, 08, 13),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "11197292"
                            }
                        }
                    }
                },
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 2,
                        TX_FirtsName = "Juan",
                        TX_MiddleName = "Jose",
                        TX_LastName = "Chacon",
                        DT_Birthday = new DateTime(1953, 12, 03),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "6789462"
                            }
                        }
                    }
                },
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 1,
                        TX_FirtsName = "Iraida",
                        TX_LastName = "Rangel",
                        DT_Birthday = new DateTime(1969, 04, 17),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "8678962"
                            }
                        }
                    }
                },
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 1,
                        TX_FirtsName = "Jessica",
                        TX_LastName = "Tovar",
                        DT_Birthday = new DateTime(1978, 03, 29),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "8233232"
                            }
                        }
                    }
                },
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 1,
                        TX_FirtsName = "Maria",
                        TX_LastName = "Rangel",
                        DT_Birthday = new DateTime(1991, 06, 18),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "22228962"
                            }
                        }
                    }
                },
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 2,
                        TX_FirtsName = "Ronner",
                        TX_LastName = "Rojas",
                        DT_Birthday = new DateTime(1969, 09, 29),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "12736832"
                            }
                        }
                    }
                },
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 2,
                        TX_FirtsName = "Michael",
                        TX_LastName = "Jackson",
                        DT_Birthday = new DateTime(1690, 05, 30),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "86732322"
                            }
                        }
                    }
                },
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 2,
                        TX_FirtsName = "Hector",
                        TX_LastName = "Lavoe",
                        DT_Birthday = new DateTime(1969, 09, 13),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "5599962"
                            }
                        }
                    }
                },
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 1,
                        TX_FirtsName = "Celia",
                        TX_LastName = "Cruz",
                        DT_Birthday = new DateTime(1946, 04, 23),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "2118962"
                            }
                        }
                    }
                },
                new Professor
                {
                    Person = new Person
                    {
                        FK_Gender = 1,
                        TX_FirtsName = "Yolanda",
                        TX_LastName = "Moreno",
                        DT_Birthday = new DateTime(1954, 07, 27),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "12777732"
                            }
                        }
                    }
                }
            };

            Context.Professors.AddRange(professors);

            List<Student> students = new List<Student>
            {
                new Student
                {
                    Person = new Person
                    {
                        FK_Gender = 2,
                        TX_FirtsName = "Jesus",
                        TX_MiddleName = "Acevedor",
                        TX_LastName = "Sandoval",
                        DT_Birthday = new DateTime(1987, 04, 23),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "2031370"
                            }
                        }
                    }
                },
                new Student
                {
                    Person = new Person
                    {
                        FK_Gender = 1,
                        TX_FirtsName = "Karla",
                        TX_MiddleName = "Angulo",
                        TX_LastName = "Perez",
                        TX_SecondLastName = "Alfonzo",
                        DT_Birthday = new DateTime(1992, 12, 24),
                        PersonalInformation = new List<PersonalInformation> {
                            new PersonalInformation {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "55503211"
                            }
                        }
                    }
                },
                 new Student
                {
                    Person = new Person
                    {
                        FK_Gender = 1,
                        TX_FirtsName = "Marisela",
                        TX_MiddleName = "Canache",
                        TX_LastName = "Sandoval",
                        DT_Birthday = new DateTime(1998, 01, 23),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "20301345"
                            }
                        }
                    }
                },
                new Student
                {
                    Person = new Person
                    {
                        FK_Gender = 1,
                        TX_FirtsName = "Eva",
                        TX_MiddleName = "Trujilllo",
                        TX_LastName = "Perez",
                        TX_SecondLastName = "",
                        DT_Birthday = new DateTime(1991, 10, 12),
                        PersonalInformation = new List<PersonalInformation> {
                            new PersonalInformation {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "20303331"
                            }
                        }
                    }
                },
                 new Student
                {
                    Person = new Person
                    {
                        FK_Gender = 2,
                        TX_FirtsName = "Cristina",
                        TX_MiddleName = "Alexandra",
                        TX_LastName = "Sandoval",
                        DT_Birthday = new DateTime(1995, 02, 21),
                        PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "44401200"
                            }
                        }
                    }
                },
                new Student
                {
                    Person = new Person
                    {
                        FK_Gender = 2,
                        TX_FirtsName = "Alexa",
                        TX_MiddleName = "Natacha",
                        TX_LastName = "Alfonzo",
                        TX_SecondLastName = "",
                        DT_Birthday = new DateTime(1954, 03, 13),
                        PersonalInformation = new List<PersonalInformation> {
                            new PersonalInformation {
                                FK_Type = 3,
                                NU_PriorityIdentification = 3,
                                TX_InformationDescription = "66603211"
                            }
                        }
                    }
                }
            };
            Context.Students.AddRange(students);
            Context.SaveChanges();
        }

        private async Task GenerateUsers()
        {
            string Email;
            string Password;

            Email = "henkalexander.sandoval@gmail.com";
            Password = "Hs20301270$";

            UserApplication user = await UserManager.FindByEmailAsync(Email);

            if (user == null)
            {
                user = new UserApplication
                {
                    UserName = Email,
                    Email = Email
                };

                await UserManager.CreateAsync(user, Password);
                UserApplication newUser = await UserManager.FindByEmailAsync(Email);

                Person person = new Person
                {
                    TX_FirtsName = "Henk",
                    TX_MiddleName = "Alexander",
                    TX_LastName = "Sandoval",
                    DT_Birthday = new DateTime(1990, 04, 23),
                    FK_Gender = 2,
                    PersonalInformation = new List<PersonalInformation>
                        {
                            new PersonalInformation
                            {
                                FK_Type = 3,
                                NU_PriorityIdentification = 1,
                                TX_InformationDescription = "20301270"
                            }
                        }
                };

                Context.Persons.Add(person);
                Context.SaveChanges();
                newUser.FK_Person = person.ID_Person;
                Context.UsersApplication.Update(newUser);
                Context.SaveChanges();
                await UserManager.AddToRoleAsync(newUser, "ESTUDIANTE");
            }

            Email = "admin_sigeao@iujo.com";
            Password = "Admin1234$";

            user = await UserManager.FindByEmailAsync(Email);

            if (user == null)
            {
                user = new UserApplication
                {
                    UserName = Email,
                    Email = Email
                };

                await UserManager.CreateAsync(user, Password);
                UserApplication newUser = await UserManager.FindByEmailAsync(Email);
                await UserManager.AddToRoleAsync(newUser, "ADMINISTRADOR");
            }

            Email = "coordinador_sigeao@iujo.com";
            Password = "Coordinador1234$";

            user = await UserManager.FindByEmailAsync(Email);

            if (user == null)
            {
                user = new UserApplication
                {
                    UserName = Email,
                    Email = Email
                };

                await UserManager.CreateAsync(user, Password);
                UserApplication newUser = await UserManager.FindByEmailAsync(Email);
                await UserManager.AddToRoleAsync(newUser, "COORDINADOR");
            }

            Email = "profesor_sigeao@iujo.com";
            Password = "Profesor_1234$";

            user = await UserManager.FindByEmailAsync(Email);

            if (user == null)
            {
                user = new UserApplication
                {
                    UserName = Email,
                    Email = Email
                };

                await UserManager.CreateAsync(user, Password);
                UserApplication newUser = await UserManager.FindByEmailAsync(Email);
                await UserManager.AddToRoleAsync(newUser, "PROFESOR");
            }
        }

        private async Task FillRoles()
        {
            List<IdentityRole> listRoles = new List<IdentityRole>
            {
                new IdentityRole { Name = "ADMINISTRADOR" },
                new IdentityRole { Name = "COORDINADOR" },
                new IdentityRole { Name = "PROFESOR" },
                new IdentityRole { Name = "ESTUDIANTE" }
            };
            foreach (var item in listRoles)
            {
                await RoleManager.CreateAsync(item);
            }
        }
    }
}