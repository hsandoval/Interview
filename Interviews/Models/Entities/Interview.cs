using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Interviews.Models.Entities {
		public class Interview {
				[Key]
                [Required]
				[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
				public int ID_Interview { get; set; }

				[Required]
				public int FK_Professor { get; set; }

				[Required]
				public int FK_Student { get; set; }

				[Required]
				public int FK_Topic { get; set; }

				[Required]
				public int FK_StateInterview { get; set; }

				[Required]
				public DateTime DT_Interview { get; set; }

				[Required]
				public string TX_DescriptionInterview { get; set; }

				public string TX_UrlInterview { get; set; }

				[Required]
				public bool BO_Enabled { get; set; }

				public DateTime? DT_Deleted { get; set; }

				#region Virtual Properties

				[ForeignKey("FK_Professor")]
				public virtual Professor Professor { get; set; } 

				[ForeignKey("FK_Student")]
				public virtual Student Student { get; set; } 

				[ForeignKey("FK_Topic")]
				public virtual Type Topic { get; set; } 

				[ForeignKey("FK_StateInterview")]
				public virtual Type StateInterview { get; set; } 
				#endregion
		}
}