﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Interviews.Models.Entities;
using Interviews.Models.Wizards;
using Microsoft.EntityFrameworkCore;

namespace Interviews.Models.Repositories {
    public interface IRepository<T> {
        T Insert(T Entity);
        bool Delete(T Entity);
        T Update(T Entity, int Id);
        IList<T> SearchWith(Func<T, bool> Predicate);
        IList<T> GetAll();
        T GetById(int Id);
        bool SaveAll();
        void Reload();
    }

    public class Repository<T> : IRepository<T> where T : class {
        static DbContextOptions<ApplicationContext> _contextOptions;
        protected ApplicationContext _EF { get; set; }
				protected readonly DbSet<T> _DbSet;

        public Repository() {
            AplicationConnection connectionString = AplicationConnection.Instance();
            _contextOptions = connectionString.DbContextOptions;

            _EF = new ApplicationContext(_contextOptions);
            _DbSet = _EF.Set<T>();
        }

        public T Insert(T Entity) {
            try {
                _DbSet.Add(Entity);
                _EF.SaveChanges();
                return Entity;
            } catch (Exception ex) {
                _DbSet.Remove(Entity);
                throw ex;
            }
        }

        public bool Delete(T Entity) {
            try {
                _DbSet.Remove(Entity);
                _EF.SaveChanges();
                return true;
            } catch (Exception) {
                _DbSet.Add(Entity);
                throw;
            }
        }

        public T GetById(int id)=> _DbSet.Find(id);

        public void Reload() {
            // var context = ((IObjectContextAdapter)_EF).ObjectContext;
            var refreshableObjects = _EF.ChangeTracker.Entries().Select(c => c.Entity).ToList();
            // context.Refresh(RefreshMode.StoreWins, refreshableObjects);
        }

        public T Update(T Entity, int Id) {
            try {
                var original = this.GetById(Id);
                if (original != null) {
                    _EF.Entry(original).CurrentValues.SetValues(Entity);
                    _EF.SaveChanges();

                    return Entity;
                }
                return null;
            } catch (Exception) {
                _DbSet.Remove(Entity);
                throw;
            }

        }

        public IList<T> SearchWith(Func<T, bool> predicate)=> _DbSet.Where(predicate).ToList();

        public IList<T> GetAll()=> _DbSet.ToList();

        public bool SaveAll()=> _EF.SaveChanges()> 0;
    }
}