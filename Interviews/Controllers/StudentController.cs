﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Caches;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class StudentController : Controller
    {
        public ILogger ILogger { get; set; }
        StudentCache studentCache = new StudentCache();

        public StudentController(ILogger<StudentController> logger)
        {
            ILogger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Estudiantes";
            ViewBag.TextButton = "Agregar nuevo estudiante";
            ViewBag.FontAwesomeIcon = "fa fa-user";
            ViewBag.Controller = "Student";
            ViewBag.Parameter = "id_Student";
            return View();
        }

        [HttpPost]
        public IActionResult TryPopulateDataTable()
        {
            try
            {
                StudentCache dataTable = studentCache.GetDataTableInformation();
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        public IActionResult Create()
        {
            ViewBag.Title = "Agregar nuevo estudiante";
            ViewBag.Controller = "Student";
            return View("_PersonForm");
        }

        [HttpPost]
        public IActionResult TrySaveChanges([FromBody] StudentViewModel viewModel)
        {
            try
            {
                StudentViewModel student = studentCache.TryEdit(viewModel);
                if (viewModel.Id_Student != 0)
                    return Ok(student);
                else
                    return Created("", student);
            }
            catch (Exception ex)
            {
                string message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        public IActionResult Edit(int id)
        {
            try
            {
                StudentViewModel student = studentCache.GetById(new StudentViewModel { Id_Student = id });
                if (student.IsNullOrEmpty())
                    throw new Exception("El registro solicitado no existe");

                ViewBag.Title = "Editar estudiante";
                ViewBag.Controller = "Student";
                return View("StudentForm", student);
            }
            catch (Exception ex)
            {
                string message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        [HttpDelete]
        public IActionResult TryDelete(int id)
        {
            try
            {
                studentCache.TryDelete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                string message = $"Error al eliminar el registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        [HttpPost]
        public IActionResult GetAll()
        {
            try
            {
                IList<StudentViewModel> students = studentCache.GetAllStudents();
                if (students.IsNullOrEmpty()) return NotFound();
                else return Ok(students);
            }
            catch (Exception ex)
            {
                string message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }
    }
}