﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Interviews.Models.Entities;

namespace Interviews.Models.ViewModels
{
    public class PersonalInformationRequiredViewModel : ViewModel<PersonalInformationRequiredViewModel>
    {
        [Display(Name = "Id")]
        public int Id_PersonalInformationRequired { get; set; }

        [Display(Name = "Descripción")]
        [StringLength(50, ErrorMessage = "El nombre de la categoria debe ser de mínimo dos (2) y máximo cincuenta (50) caracteres.", MinimumLength = 2)]
        [Required(ErrorMessage = "El nombre de la categoria es requerido")]
        public string Tx_Description { get; set; }

        [Display(Name = "Categoria asociada")]
        [Required(ErrorMessage = "El nombre de la categoria es requerido")]
        public int Fk_AssociatedCategory { get; set; }

        public string Tx_AssociatedCategory { get; set; }

        [Display(Name = "Select")]
        public bool Bo_IsDropDownList { get; set; }

        [Display(Name = "Eliminado")]
        public bool Bo_Deleted { get { return this.Dt_Deleted.HasValue; } }

        bool Bo_IncludeRegistersDeleted { get; set; }

        public PersonalInformationRequiredViewModel() { }

        public PersonalInformationRequiredViewModel(PersonalInformationRequired personalInformationRequired)
        {
            if (personalInformationRequired == null)
            {
                return;
            }
            else
            {
                Bo_IsEmpty = false;
                Id_PersonalInformationRequired = personalInformationRequired.ID_PersonalInformationRequired;
                Tx_Description = personalInformationRequired.TX_Description;
                Fk_AssociatedCategory = personalInformationRequired.FK_AssociatedCategory;
                Bo_IsDropDownList = personalInformationRequired.BO_IsDropDownList;
                Bo_Enabled = personalInformationRequired.BO_Enabled;
                Dt_Deleted = personalInformationRequired.DT_Deleted;
                if (personalInformationRequired.Category != null) this.Tx_AssociatedCategory = personalInformationRequired.Category.TX_Description;
            }
        }

        public PersonalInformationRequired ConvertViewModelToClass() => new PersonalInformationRequired
        {
            ID_PersonalInformationRequired = this.Id_PersonalInformationRequired,
            TX_Description = this.Tx_Description,
            FK_AssociatedCategory = this.Fk_AssociatedCategory,
            BO_IsDropDownList = this.Bo_IsDropDownList,
            BO_Enabled = this.Bo_Enabled,
            DT_Deleted = this.Dt_Deleted,
        };

        public void GetActions() => this.Action = new Action(this.Id_PersonalInformationRequired);
    }
}