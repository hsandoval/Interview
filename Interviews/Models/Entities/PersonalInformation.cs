using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Interviews.Models.Entities {
		public class PersonalInformation {
				[Key]
				[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
				public int ID_PersonalInformation { get; set; }

				public string TX_InformationDescription { get; set; }

				public int FK_Type { get; set; }

				public int FK_Person { get; set; }

				public int? NU_PriorityIdentification { get; set; }

				[Required]
				public bool BO_Enabled { get; set; }

				public DateTime? DT_Deleted { get; set; }

				[ForeignKey("FK_Type")]
				public virtual Type Type { get; set; }

				[ForeignKey("FK_Person")]
				public virtual Person Person { get; set; } 
		}
}