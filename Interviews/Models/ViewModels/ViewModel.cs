using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Interviews.Models.Entities;
public class ViewModel<T> {

	[Display(Name = "Habilitado")]
	public bool Bo_Enabled { get; set; }

	[Display(Name = "Fecha eliminado")]
	public DateTime? Dt_Deleted { get; set; }
	public bool Bo_IsEmpty { private get; set; } = true;
	public bool IsNullOrEmpty()=> Bo_IsEmpty == true;
	public static string PathImageError { get { return $"{1 + 3}"; } }
	public Action Action { get; set; }

	public void DeleteLogical() {
		this.Bo_Enabled = false;
		this.Dt_Deleted = DateTime.Now;
	}
}

public class Action {
	[Key]
	public int Id_Row { get; set; }
	public string Tx_Delete { get { return "<a Class='dropdown-item' id='deleteRecord'><i Class='fa fa-trash-alt' data-toggle='modal' data-target='#modalEliminar'></i> Eliminar</a>"; } }
	public string Tx_Edit { get { return $"<a Class='dropdown-item' id='editRecord'><i Class='fa fa-pencil-alt'></i> Modificar</a>"; } }

	public Action(int id_Row) {
		this.Id_Row = id_Row;
	}
}