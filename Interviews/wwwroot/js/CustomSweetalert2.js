let ConfirmDeleteAction = () => {
	swal({
		title: '¿Está seguro de eliminar este registro?',
		text: '¡No podrás revertir esto!',
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí, eliminarlo!'
	}).then((result) => {
		if (result.value) {
			swal(
				'¡Eliminado!',
				'El registro ha sido eliminado.',
				'success'
			);
		} else {
			swal(
				'¡Cancelado!',
				'El registro se encuentra a salvo.',
				'error'
			);
		}
	});
}


let ConfirmInsertActionReturnData = (urlRequest, obj) => {
    return new Promise((resolve, rejected) => {
        swal({
            title: 'Ejecutar cambios',
            text: '¿Está seguro de editar este registro?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, ejecutar!'
        }).then((result) => {
            if (result.value) {

                let HttpRequest = MakeRequest(urlRequest, obj);

                HttpRequest
                    .then(res => {
                        if ((res.status === 201 || res.status === 200)) {
                            swal(
                                'Procesado!',
                                'Los cambios se efectuaron exitosamente.',
                                'success'
                            );
                            return resolve(res);
                        } else {
                            swal(
                                'Errores!',
                                'La solicitud no pudo ser procesada.',
                                'info'
                            );
                            return resolve(res)
                        }
                    }).catch(err => {
                        swal(
                            'Error!',
                            'Ha ocurrido un error de conexión.',
                            'error'
                        );
                        return rejected(err);
                    });

            } else {
                swal(
                    '¡Cancelado!',
                    'Se cancelo la acción.',
                    'info'
                );
                return rejected('Acción cancelada');
            }
        });
    });
};

let ConfirmInsertAction = (urlRequest, obj) => {
    swal({
        title: 'Ejecutar cambios',
        text: '¿Está seguro de editar este registro?',
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, ejecutar!'
    }).then((result) => {
        if (result.value) {

            let HttpRequest = MakeRequest(urlRequest, obj);

            HttpRequest
                .then(res => {
                    if ((res.status === 201 || res.status === 200)) {
                        swal(
                            'Procesado!',
                            'Los cambios se efectuaron exitosamente.',
                            'success'
                        );
                        return res;
                    } else {
                        swal(
                            'Errores!',
                            'La solicitud no pudo ser procesada.',
                            'info'
                        );
                    }
                }).catch(err => {
                    swal(
                        'Error!',
                        'Ha ocurrido un error de conexión.',
                        'error'
                    );
                });

        } else {
            swal(
                '¡Cancelado!',
                'Se cancelo la acción.',
                'info'
            );
        }
    });
};