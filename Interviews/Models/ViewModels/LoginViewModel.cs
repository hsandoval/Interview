﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.ViewModels
{
    public class LoginViewModel
    {

        [Required]
        [StringLength(100, ErrorMessage = "La contraseña debe tener mínimo {2} y máximo {1} caracteres de longitud.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "La contraseña y su confirmación deben coincidir.")]
        public string ConfirmPassword { get; set; }


        public bool RememberMe { get; set; }

        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
