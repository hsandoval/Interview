﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;

namespace Interviews.Models.Caches {

	public class IconFontAwesomeCache : Cache<IconFontAwesomeViewModel> {

		readonly IIconFontAwesomeRepository repository = new IconFontAwesomeRepository();

		public IList<IconFontAwesomeViewModel> GetAll()=>
		repository.GetAll().Select(x => new IconFontAwesomeViewModel(x)).ToList();

	}
}