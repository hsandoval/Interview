﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.ViewModels
{
    public class FullCalendarViewModel
    {
        public string Id { get; set; }
        public int Fk_Professor { get; set; }
        public string Title { get; set; }
        public string Constraint { get; set; }
        public string Color { get; set; }
        public string Rendering { get; set; }
        public bool Overlap { get; set; } 
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public Boolean Allday { get; set; }

        public FullCalendarViewModel()
        {
        }

        public SheduleByProfessorViewModel ConvertToSheduleByProfessorViewModel() =>
                new SheduleByProfessorViewModel
                {
                    Dt_From = this.Start,
                    Dt_To = this.End,
                    Fk_Professor = this.Fk_Professor
                };
    }
}
