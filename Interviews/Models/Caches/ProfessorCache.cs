﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;

namespace Interviews.Models.Caches {
		public class ProfessorCache : Cache<ProfessorViewModel> {
				readonly IProfessorRepository repository = new ProfessorRepository();

				public ProfessorCache GetDataTableInformation() {
						IList<ColumnDataTable> columns = GetColumnDataTable();
						IList<ProfessorViewModel> professors = SearchWith(x => !x.DT_Deleted.HasValue);
						professors.ToList().ForEach(x => x.GetActions());
						return new ProfessorCache() {
								ListColumnsDataTable = columns,
								ListData = professors
						};
				}

				private IList<ColumnDataTable> GetColumnDataTable() =>
				new List<ColumnDataTable>() {
			new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_Person", Tx_ColumnTitle = "Id" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_FullName", Tx_ColumnTitle = "Nombre" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "dt_Birthday", Tx_ColumnTitle = "Fecha Nacimiento" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "nu_Age", Tx_ColumnTitle = "Edad" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
				};

				public IList<ProfessorViewModel> SearchWith(Func<Professor, bool> Predicate) =>
					repository.SearchWithProfessor(Predicate).Select(x => new ProfessorViewModel(x)).ToList();

				public ProfessorViewModel TryEdit(ProfessorViewModel viewModel) {
						ProfessorViewModel currentRecord = GetById(viewModel);
						ProfessorViewModel returnedRecord;

						if (currentRecord.IsNullOrEmpty())
								returnedRecord = Insert(viewModel);
						else
								returnedRecord = Update(viewModel);

						return returnedRecord;
				}

				public ProfessorViewModel GetById(ProfessorViewModel viewModel) =>
				new ProfessorViewModel(repository.SearchWithProfessor(x => x.ID_Professor == viewModel.Id_Professor).SingleOrDefault());

				ProfessorViewModel Insert(ProfessorViewModel viewModel) {

						viewModel.Person.PersonalInformation = new List<PersonalInformationViewModel> {
								new PersonalInformationViewModel {
										Nu_PriorityIdentification = 1,
										Fk_TypeInformation = viewModel.Person.Fk_TypeDocumentIdentification,
										Tx_InformationDescription = viewModel.Person.Tx_DescriptionDocumentIdentification
								}
						};

						return new ProfessorViewModel(repository.Insert(viewModel.ConvertViewModelProfessorToClass()));
				}

				ProfessorViewModel Update(ProfessorViewModel viewModel) =>
				new ProfessorViewModel(repository.Update(viewModel.ConvertViewModelProfessorToClass(), viewModel.Id_Professor));

				public bool TryDelete(int id_Professor, bool DeleteLogical = true) {
						ProfessorViewModel currentRecord = GetById(new ProfessorViewModel { Id_Professor = id_Professor });
						if (!currentRecord.IsNullOrEmpty()) {
								if (DeleteLogical)
										return LogicDelete(currentRecord);
								else
										return PhysicalDelete(currentRecord);
						}

						return false;
				}

				bool LogicDelete(ProfessorViewModel viewModel) {
						viewModel.DeleteLogical();
						repository.Update(viewModel.ConvertViewModelProfessorToClass(), OwnHelper.ConvertStringToInt(viewModel.Id_Person));
						return true;
				}

				bool PhysicalDelete(ProfessorViewModel viewModel) =>
				repository.Delete(viewModel.ConvertViewModelProfessorToClass());

				public IList<ProfessorViewModel> GetAllProfessors() =>
						repository.GetAllProfessors().Select(x => new ProfessorViewModel(x)).ToList();
		}
}