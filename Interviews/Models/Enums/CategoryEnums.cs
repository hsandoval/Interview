﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Enums {
		public enum CategoryEnums {
				Localidades = 1,
				Pais = 2,
				Estado = 3,
				Ciudad = 4,
				Sector = 5,
				Departamento = 6,
				Corregimiento = 7,
				Topicos = 8,
				Generos = 9,
				DocumentosIdentificación = 10,
				RedesSociales = 11,
                Materias = 13,
				EstatusEntrevistas = 12,
				Roles = 14,
		}
}