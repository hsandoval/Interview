using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Caches;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class IconFontAwesomeController : Controller
    {

        public ILogger ILogger { get; set; }
        public IconFontAwesomeController(ILogger<IconFontAwesomeController> Logger)
        {
            ILogger = Logger;
        }


        [HttpPost]
        public IActionResult GetAll()
        {
            IconFontAwesomeCache cache = new IconFontAwesomeCache();

            try {
                IList<IconFontAwesomeViewModel> icons = cache.GetAll();

            if (icons.IsNullOrEmpty())
                return NotFound();
            else
                return Ok(icons);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }         
    }
}