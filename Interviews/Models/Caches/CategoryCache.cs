﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;

namespace Interviews.Models.Caches
{

    public class CategoryCache : Cache<CategoryViewModel>
    {

        readonly ICategoryRepository repository = new CategoryRepository();

        public IList<TreeViewViewModel> TreeHierarchy { get; set; }

        public CategoryCache GetDataTableInformation()
        {
            IList<ColumnDataTable> columns = GetColumnDataTable();
            IList<CategoryViewModel> categories = SearchWith(x => !x.DT_Deleted.HasValue);
            categories.ToList().ForEach(x => x.GetActions());
            return new CategoryCache()
            {
                ListColumnsDataTable = columns,
                ListData = categories
            };
        }

        private IList<ColumnDataTable> GetColumnDataTable() =>
        new List<ColumnDataTable>() {
            new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "id_Category", Tx_ColumnTitle = "Id" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_Description", Tx_ColumnTitle = "Descripción" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "bo_Enabled", Tx_ColumnTitle = "Habilitado" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "bo_IsTypeFather", Tx_ColumnTitle = "Categoría maestra" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
        };

        public CategoryViewModel GetById(int idCategory) =>
        new CategoryViewModel(repository.GetById(idCategory));

        public CategoryViewModel TryEdit(CategoryViewModel viewModel)
        {
            CategoryViewModel currentRecord = GetById(viewModel.Id_Category);

            if (currentRecord.IsNullOrEmpty())
            {
                return Insert(viewModel);
            }
            else
            {
                Update(viewModel);
                return viewModel;
            }
        }

        CategoryViewModel Insert(CategoryViewModel viewModel) =>
        new CategoryViewModel(repository.Insert(viewModel.ConvertViewModelToClass()));

        public void Delete(int idCategory, bool isDeleteLogical = true)
        {
            Category currentRecord = repository.GetById(idCategory);

            if (currentRecord != null)
            {

                var viewModel = new CategoryViewModel(currentRecord);

                if (isDeleteLogical)
                {
                    DeleteLogical(viewModel);
                    return;
                }
                DeletePhysical(viewModel);
            }
        }

        CategoryViewModel Update(CategoryViewModel viewModel) =>
        new CategoryViewModel(repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_Category));

        public IList<CategoryViewModel> GetAllMenuOptions() => GetAllCategories().Where(x => x.Bo_IsTypeFather).ToList();

        public CategoryViewModel GetCategoriesByMainCategory(CategoryEnums mainCategory) =>
        new CategoryViewModel(GetCategoriesByPredicate(x => x.ID_Category == mainCategory.GetHashCode()));

        public IList<TreeViewViewModel> GetListChildren(CategoryViewModel viewModel)
        {
            CategoryViewModel category = new CategoryViewModel(repository.GetCategoriesByPredicate(x =>
                x.ID_Category == viewModel.Id_Category
            ).SingleOrDefault());

            this.TreeHierarchy = new List<TreeViewViewModel>();

            category.ChildrenTypes.ToList().ForEach(x => OrderTree(x));

            return TreeHierarchy;
        }

        public IList<TreeViewViewModel> GetAllTreViewViewModel()
        {
            IList<CategoryViewModel> categories = repository.GetAllCategories().Select(x => new CategoryViewModel(x)).ToList();

            this.TreeHierarchy = new List<TreeViewViewModel>();

            categories.ToList().ForEach(category =>
            {
                if (!category.ChildrenTypes.IsNullOrEmpty())
                {
                    category.ChildrenTypes.ToList().ForEach(x => OrderTree(x));
                }
            });

            return TreeHierarchy;
        }

        public CategoryViewModel GetCategories() =>
        new CategoryViewModel()
        {
            Tx_Description = "Categories",
            Categories = repository.GetAll().Select(x => new CategoryViewModel(x)).ToList()
        };

        public IList<CategoryViewModel> SearchWith(Func<Category, bool> predicate) =>
        repository.SearchWith(predicate).Select(x => new CategoryViewModel(x)).ToList();

        Category GetCategoriesByPredicate(Func<Category, bool> predicate) =>
        repository.GetCategoriesByPredicate(predicate).SingleOrDefault();

        void DeleteLogical(CategoryViewModel viewModel)
        {
            viewModel.Dt_Deleted = DateTime.Now;
            viewModel.Bo_Enabled = false;
            repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_Category);
        }

        void DeletePhysical(CategoryViewModel viewModel) =>
        repository.Delete(viewModel.ConvertViewModelToClass());

        void OrderTree(TypeViewModel type)
        {
            if (!type.ChildrenTypes.IsNullOrEmpty())
            {
                type.ChildrenTypes.ToList().ForEach(x => OrderTree(x));
            }
            this.TreeHierarchy.Add(new TreeViewViewModel(type));
        }

        public IList<CategoryViewModel> GetAllCategories() =>
            repository.GetAll().Select(x => new CategoryViewModel(x)).ToList();

        public CategoryViewModel GetCategoriesFathersAndChildren(CategoryViewModel viewModel) =>
        new CategoryViewModel(GetCategoriesByPredicate(x => x.ID_Category == viewModel.Id_Category));

    }
}