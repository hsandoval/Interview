﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Caches;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class ProfessorController : Controller
    {
        public ILogger ILogger { get; set; }
        ProfessorCache professorCache = new ProfessorCache();

        public ProfessorController(ILogger<ProfessorController> logger)
        {
            ILogger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Profesores";
            ViewBag.TextButton = "Agregar nuevo profesor";
            ViewBag.FontAwesomeIcon = "fa fa-user";
            ViewBag.Controller = "Professor";
            ViewBag.Parameter = "id_Professor";
            return View();
        }

        [HttpPost]
        public IActionResult TryPopulateDataTable()
        {
            try
            {
                ProfessorCache dataTable = professorCache.GetDataTableInformation();
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        public IActionResult Create()
        {
            ViewBag.Title = "Agregar nuevo profesor";
            ViewBag.Controller = "Professor";
            return View("ProfessorForm");
        }

        [HttpPost]
        public IActionResult TrySaveChanges([FromBody] ProfessorViewModel viewModel)
        {
            try
            {
                ProfessorViewModel professor = professorCache.TryEdit(viewModel);
                if (viewModel.Id_Professor != 0)
                    return Ok(professor);
                else
                    return Created("", professor);
            }
            catch (Exception ex)
            {
                string message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        [HttpPost]
        public IActionResult TryGetById([FromBody] ProfessorViewModel viewModel)
        {
            try
            {
                ProfessorViewModel professor = professorCache.GetById(viewModel);
                if (professor.IsNullOrEmpty())
                    return NotFound();
                else
                    return Ok(professor);
            }
            catch (Exception ex)
            {
                string message = $"Error al obtener el registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        public IActionResult Edit(int id)
        {
            try
            {
                ProfessorViewModel professor = professorCache.GetById(new ProfessorViewModel { Id_Professor = id });

                if (professor.IsNullOrEmpty())
                    throw new Exception("El registro solicitado no existe");

                ViewBag.Title = "Editar profesor";
                ViewBag.Controller = "Professor";
                return View("ProfessorForm", professor);
            }
            catch (Exception ex)
            {
                string message = $"Error al obtener el registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        [HttpDelete]
        public IActionResult TryDelete(int id)
        {
            try
            {
                professorCache.TryDelete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                string message = $"Error al eliminar el registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        [HttpPost]
        public IActionResult GetAll()
        {
            try
            {
                IList<ProfessorViewModel> professors = professorCache.GetAllProfessors();
                if (professors.IsNullOrEmpty())
                    return NotFound();
                else
                    return Ok(professors);
            }
            catch (Exception ex)
            {
                string message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        [HttpPost]
        public IActionResult GetAllDataFullCalendarByProfessor([FromBody]ProfessorViewModel professor)
        {
            SheduleByProfessorCache cache = new SheduleByProfessorCache();
            try
            {
                IList<FullCalendarViewModel> data = cache.GetAllFullCalendarByIdProfesor(new SheduleByProfessorViewModel { Fk_Professor = professor.Id_Professor });
                return Ok(data);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost]
        public IActionResult GetAllDataAvalaibleFullCalendarByProfessor([FromBody] ProfessorViewModel professor)
        {
            SheduleByProfessorCache cache = new SheduleByProfessorCache();
            try
            {
                IList<FullCalendarViewModel> data = cache.GetAllDataAvalaibleFullCalendarByProfessor(new SheduleByProfessorViewModel { Fk_Professor = professor.Id_Professor });
                if (data.IsNullOrEmpty())
                    return NotFound();
                else
                    return Ok(data.OrderBy(x => x.Start));
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }
    }
}