using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Interviews.Models.Entities {
	public class Type {
		public Type() {
			this.ChildrenTypes = new HashSet<Type>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ID_Type { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 1)]
		public string TX_Description { get; set; }

		public int FK_IconFontAwesome { get; set; }

		public int FK_Category { get; set; }

		public int? FK_ParentType { get; set; }

		[Required]
		public bool BO_Enabled { get; set; }

		public DateTime? DT_Deleted { get; set; }

		[ForeignKey("FK_Category")]
		public virtual Category Category { get; set; }

		[ForeignKey("FK_ParentType")]
		public virtual Type ParentType { get; set; }

        [ForeignKey("FK_IconFontAwesome")]
        public virtual IconFontAwesome IconFontAwesome { get; set; }

        public virtual ICollection<Type> ChildrenTypes { get; set; }

	}
}