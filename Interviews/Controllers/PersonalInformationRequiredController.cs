using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Caches;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class PersonalInformationRequiredController : Controller
    {

        public ILogger ILogger { get; set; }

        PersonalInformationRequiredCache personalInformationRequiredCache = new PersonalInformationRequiredCache();

        public PersonalInformationRequiredController(ILogger<PersonalInformationRequiredController> logger)
        {
            ILogger = logger;
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Informacion Personal sugerida";
            ViewBag.TextButton = " Agregar nueva información";
            ViewBag.FontAwesomeIcon = "far fa-user-circle";
            ViewBag.Controller = "PersonalInformationRequierd";
            ViewBag.Parameter = "id_PersonalInformationRequierd";
            return View();
        }

        [HttpPost]
        public IActionResult TryPopulateDataTable()
        {
            try
            {
                PersonalInformationRequiredCache dataTable = personalInformationRequiredCache.GetDataTableInformation();
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        public IActionResult Create()
        {
            ViewBag.Title = "Agregar nuevo profesor";
            ViewBag.Controller = "Professor";
            return View("ProfessorForm");
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public IActionResult TrySaveChanges([FromBody] PersonalInformationRequiredViewModel viewModel)
        {
            try
            {
                PersonalInformationRequiredViewModel personalInformationRequired = personalInformationRequiredCache.TryEdit(viewModel);
                if (viewModel.Id_PersonalInformationRequired != 0)
                    return Ok(personalInformationRequired);
                else
                    return Created("", personalInformationRequired);
            }
            catch (Exception ex)
            {
                string message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public IActionResult TryGetById([FromBody] PersonalInformationRequiredViewModel viewModel)
        {
            try
            {
                PersonalInformationRequiredViewModel personalInformationRequired = personalInformationRequiredCache.GetById(viewModel);
                if (personalInformationRequired.IsNullOrEmpty())
                    return NotFound();
                else
                    return Ok(personalInformationRequired);
            }
            catch (Exception ex)
            {
                string message = $"Error al obtener el registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        public IActionResult Edit(int id)
        {
            try
            {
                PersonalInformationRequiredViewModel personalInformationRequired = personalInformationRequiredCache.GetById(new PersonalInformationRequiredViewModel { Id_PersonalInformationRequired = id });

                if (personalInformationRequired.IsNullOrEmpty())
                    throw new Exception("El registro solicitado no existe");

                ViewBag.Title = "Editar Informacion";
                ViewBag.Controller = "PersonalInformationRequired";
                return View("PersonalInformationRequiredForm", personalInformationRequired);
            }
            catch (Exception ex)
            {
                string message = $"Error al obtener el registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        [HttpDelete]
        // [ValidateAntiForgeryToken]
        public IActionResult TryDelete(int id)
        {
            try
            {
                personalInformationRequiredCache.TryDelete(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                string message = $"Error al eliminar el registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }
    }
}