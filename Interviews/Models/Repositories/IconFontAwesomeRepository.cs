﻿using Interviews.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Repositories {
	public interface IIconFontAwesomeRepository : IRepository<IconFontAwesome> { }
	public class IconFontAwesomeRepository : Repository<IconFontAwesome>, IIconFontAwesomeRepository { }
}
