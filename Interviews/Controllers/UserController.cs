using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Caches;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class UserController : Controller
    {
        public ILogger ILogger { get; set; }
        UserCache userCache = new UserCache();

        public UserController(ILogger<UserController> logger)
        {
            ILogger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Usuarios";
            ViewBag.TextButton = "Agregar nuevo usuario";
            ViewBag.FontAwesomeIcon = "fa fa-user";
            ViewBag.Controller = "User";
            ViewBag.Parameter = "id_User";
            return View();
        }

        [HttpPost]
        public IActionResult TryPopulateDataTable()
        {
            try
            {
                UserCache dataTable = userCache.GetDataTableInformation();
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        public IActionResult Create()
        {
            ViewBag.Title = "Agregar nuevo usuario";
            ViewBag.Controller = "User";
            return View("UserForm");
        }

        //[HttpPost]
        //public IActionResult TrySaveChanges([FromBody] UserViewModel viewModel)
        //{
        //    try
        //    {
        //        UserViewModel user = userCache.TryEdit(viewModel);
        //        if (viewModel.Id_User != 0)
        //            return Ok(user);
        //        else
        //            return Created("", user);
        //    }
        //    catch (Exception ex)
        //    {
        //        string message = $"Error al insertar un nuevo registro: error -> {ex}";
        //        ILogger.LogError(message);
        //        return BadRequest(message);
        //    }
        //}

        [HttpPost]
        public IActionResult TryGetById([FromBody] UserViewModel viewModel)
        {
            try
            {
                UserViewModel user = userCache.GetById(viewModel);
                if (user.IsNullOrEmpty())
                    return NotFound();
                else
                    return Ok(user);
            }
            catch (Exception ex)
            {
                string message = $"Error al obtener el registro: error -> {ex}";
                ILogger.LogError(message);
                return BadRequest(message);
            }
        }

        //public IActionResult Edit(int id)
        //{
        //    try
        //    {
        //        UserViewModel user = userCache.GetById(new UserViewModel { Id_User = id });

        //        if (user.IsNullOrEmpty())
        //            throw new Exception("El registro solicitado no existe");

        //        ViewBag.Title = "Editar usuario";
        //        ViewBag.Controller = "User";
        //        return View("UserForm", user);
        //    }
        //    catch (Exception ex)
        //    {
        //        string message = $"Error al obtener el registro: error -> {ex}";
        //        ILogger.LogError(message);
        //        return BadRequest(message);
        //    }
        //}

        //[HttpDelete]
        //public IActionResult TryDelete(int id)
        //{
        //    try
        //    {
        //        userCache.TryDelete(id);
        //        return NoContent();
        //    }
        //    catch (Exception ex)
        //    {
        //        string message = $"Error al eliminar el registro: error -> {ex}";
        //        ILogger.LogError(message);
        //        return BadRequest(message);
        //    }
        //}

        //[HttpPost]
        //public IActionResult GetAll()
        //{
        //    try
        //    {
        //        IList<UserViewModel> user = userCache.GetAllUsers();
        //        if (user.IsNullOrEmpty())
        //            return NotFound();
        //        else
        //            return Ok(user);
        //    }
        //    catch (Exception ex)
        //    {
        //        string message = $"Error al insertar un nuevo registro: error -> {ex}";
        //        ILogger.LogError(message);
        //        return BadRequest(message);
        //    }
        //}
    }
}