﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Enums {
	public static class ConnectionStringEnum {
		public static string HenkLinuxPostgres = "User Id=postgres;Password=Hs20301270$;Server=localhost;Port=5432;Database=Interview;Integrated Security=true;Pooling=true;";
		public static string HenkLinuxMySql = "User Id=sa;Password=Hs20301270$;Server=localhost;Port=5432;Database=Interview;Integrated Security=true;Pooling=true;";
		public static string HenkWindowsLaptop = "Data Source = HP-Windows\\SQLEXPRESS;Initial Catalog = Interview; Integrated Security = False; User Id = sa; Password=Hs20301270$;MultipleActiveResultSets=True";
		public static string HenkWindowsPC = "Data Source=localhost\\SQL2016;Initial Catalog=Interview;Integrated Security=False;User Id=sa;Password=keycore9652;MultipleActiveResultSets=True";
		public static string Jordan = "Data Source=JORDANPEREZ\\SQL2017;Initial Catalog=Interview;Integrated Security=False;User Id=sa;Password=089010sql;MultipleActiveResultSets=True";

		public enum CountryEnum {
			[Display(Name = "United Mexican States")]
			Mexico, [Display(Name = "United States of America")]
			USA,
			Canada,
			France,
			Germany,
			Spain
		}

	}
}