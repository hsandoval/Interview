﻿using System;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Interviews.Models.Wizards
{
    public static class OwnHtmlHelper
    {
        public static IHtmlContent Ternary(this IHtmlHelper htmlHelper, bool condition, string ifTrue, string   ifFalse)
            => new HtmlString(condition ? ifTrue : ifFalse);

        public static IHtmlContent HelloWorldHTMLString(this IHtmlHelper htmlHelper)
            => new HtmlString("<strong>Hello World</strong>");
        public static String HelloWorldString(this IHtmlHelper htmlHelper)
            => "<strong>Hello World</strong>";
    }
}
