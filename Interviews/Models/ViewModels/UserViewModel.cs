using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Interviews.Models.Entities;
using Interviews.Models.Wizards;
using MoreLinq;

namespace Interviews.Models.ViewModels
{
    public class UserViewModel : PersonViewModel
    {
        public UserViewModel()
        {

        }
        public UserViewModel(Person person)
        {
            if (person == null)
            {
                return;
            }
            else
            {
            }

        }

        public Professor ConvertViewModelProfessorToClass() => new Professor
        {
        };

        public void GetActionsProfessor() => this.Action = new Action(1);
    }
}