﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;

namespace Interviews.Models.Caches
{
    public class SheduleByProfessorCache : Cache<SheduleByProfessorViewModel>
    {
        const string TX_CONSTRAINT = "availableForMeeting";
        readonly ISheduleByProfessorRepository repository = new SheduleByProfessorRepository();

        public SheduleByProfessorCache GetDataTableInformationByIdProfessor(SheduleByProfessorViewModel shedule)
        {
            IList<ColumnDataTable> columns = GetColumnDataTable();
            IList<SheduleByProfessorViewModel> sheduleByProfessor = SearchWith(x => !x.DT_Deleted.HasValue && x.FK_Professor == shedule.Fk_Professor).ToList();
            sheduleByProfessor.ToList().ForEach(x => x.GetActions());
            return new SheduleByProfessorCache()
            {
                ListColumnsDataTable = columns,
                ListData = sheduleByProfessor
            };
        }

        private IList<ColumnDataTable> GetColumnDataTable() =>
        new List<ColumnDataTable>() {
                    new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_SheduleByProfessor", Tx_ColumnTitle = "Id" },
                    new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "dt_From", Tx_ColumnTitle = "Fecha desde" },
                    new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "dt_To", Tx_ColumnTitle = "Fecha hasta" },
                    new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
        };

        internal void Delete(int iD_Record)
        {
            throw new NotImplementedException();
        }

        public IList<SheduleByProfessorViewModel> GetAll() =>
            repository.GetAll().Select(x => new SheduleByProfessorViewModel(x)).ToList();

        public IList<SheduleByProfessorViewModel> SearchWith(Func<SheduleByProfessor, bool> Predicate) =>
            repository.SearchWith(Predicate).Select(x => new SheduleByProfessorViewModel(x)).ToList();

        public SheduleByProfessorViewModel TryEdit(SheduleByProfessorViewModel viewModel)
        {
            SheduleByProfessorViewModel currentRecord = GetById(viewModel);
            SheduleByProfessorViewModel returnedRecord;

            if (currentRecord.IsNullOrEmpty())
                returnedRecord = Insert(viewModel);
            else
                returnedRecord = Update(viewModel);

            return returnedRecord;
        }

        public SheduleByProfessorViewModel GetById(SheduleByProfessorViewModel viewModel) =>
        new SheduleByProfessorViewModel(repository.SearchWith(x => x.ID_SheduleByProfessor == viewModel.Id_SheduleByProfessor).SingleOrDefault());


        SheduleByProfessorViewModel Insert(SheduleByProfessorViewModel viewModel)
        {

            return new SheduleByProfessorViewModel(repository.Insert(viewModel.ConvertViewModelToClass()));
        }

        SheduleByProfessorViewModel Update(SheduleByProfessorViewModel viewModel) =>
        new SheduleByProfessorViewModel(repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_SheduleByProfessor));



        public IList<FullCalendarViewModel> GetAllFullCalendarByIdProfesor(SheduleByProfessorViewModel viewModel) =>
             repository.GetSheduleByProfessor(viewModel.ConvertViewModelToClass()).Select(x => new FullCalendarViewModel { Id = x.ID_SheduleByProfessor.ToString(), Start = x.DT_From, End = x.DT_To, Title = "Disponible" }).ToList();

        public IList<FullCalendarViewModel> GetAllDataAvalaibleFullCalendarByProfessor(SheduleByProfessorViewModel viewModel)
        {
            List<FullCalendarViewModel> data = GetAllFullCalendarByIdProfesor(viewModel).ToList();

            return ParseAllEventsAvalaibleAndConstraint(data);
        }

        public List<FullCalendarViewModel> ParseAllEventsAvalaibleAndConstraint(List<FullCalendarViewModel> events)
        {

            List<FullCalendarViewModel> eventsOrderAscendent = events.OrderBy(x => x.Start).ToList();

            if (eventsOrderAscendent.IsNullOrEmpty()) return BlockAllEvents();
            else return ParseEventsAvailable(eventsOrderAscendent);
        }


        private static List<FullCalendarViewModel> ParseEventsAvailable(List<FullCalendarViewModel> eventsOrderAscendent)
        {
            DateTime now = DateTime.Now;
            DateTime auxiliarDate = new DateTime(now.Year, 1, 1);
            List<FullCalendarViewModel> dateConstraint = new List<FullCalendarViewModel> {
                new FullCalendarViewModel
                {
                    Title = "Hora actual",
                    Constraint = TX_CONSTRAINT,
                    Color = "#257e4a",
                    Start = now,
                    End = now.AddMinutes(30)
                }
            };

            eventsOrderAscendent.ForEach(x =>
            {
                dateConstraint.Add(new FullCalendarViewModel
                {
                    Start = auxiliarDate,
                    End = x.Start,
                    Overlap = false,
                    Rendering = "background",
                    Color = "#ff9f89"
                });
                dateConstraint.Add(new FullCalendarViewModel
                {
                    Id = TX_CONSTRAINT,
                    Start = x.Start,
                    End = x.End,
                    Overlap = true,
                    Rendering = "background",
                    Color = "#257e4a",
                });
                auxiliarDate = x.End;
            });

            dateConstraint.Add(new FullCalendarViewModel
            {
                Id = TX_CONSTRAINT,
                Start = dateConstraint.Max(x => x.End),
                End = new DateTime(now.Year + 1, 12, 31),
                Overlap = true,
                Rendering = "background",
                Color = "#ff9f89",
            });

            return dateConstraint;
        }

        private List<FullCalendarViewModel> BlockAllEvents()
        {
            DateTime now = DateTime.Now;
            var data = new List<FullCalendarViewModel> {
                new FullCalendarViewModel
                {
                    Title = "Hora actual",
                    Constraint = TX_CONSTRAINT,
                    Color = "#257e4a",
                    Start = now,
                    End = now.AddMinutes(30)
                },
                new FullCalendarViewModel
                {
                    Start = new DateTime(now.Year - 1, 1, 1),
                    End = new DateTime(now.Year + 1, 12, 31),
                    Overlap = false,
                    Rendering = "background",
                    Color = "#ff9f89"
                }
            };
            return data;
        }
    }
}