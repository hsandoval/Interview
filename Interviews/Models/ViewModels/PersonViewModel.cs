﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Wizards;

namespace Interviews.Models.ViewModels
{
    public class PersonViewModel : ViewModel<PersonViewModel>
    {
        public string Id_Person { get; set; }

        [Required]
        [Display(Name = "Primer Nombre")]
        [StringLength(50, ErrorMessage = "Debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 2)]
        public string Tx_FirtsName { get; set; }

        [Required]
        [Display(Name = "Segundo Nombre")]
        [StringLength(50, ErrorMessage = "Debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 2)]
        public string Tx_MiddleName { get; set; }

        [Required]
        [Display(Name = "Primer Apellido")]
        [StringLength(50, ErrorMessage = "Debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 2)]
        public string Tx_LastName { get; set; }

        [Required]
        [Display(Name = "Segundo Apellido")]
        [StringLength(50, ErrorMessage = "Debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 2)]
        public string Tx_SecondLastName { get; set; }

        [Required]
        [Display(Name = "Fecha nacimiento")]
        public DateTime Dt_Birthday { get; set; }

        [Required]
        [Display(Name = "Edad")]
        public int Nu_Age { get { return DateTime.Now.AddTicks(0 - Dt_Birthday.Ticks).Year - 1; } }

        [Required]
        [Display(Name = "Nombre completo")]
        public string Tx_FullName { get { return $"{Tx_FirtsName} {Tx_MiddleName} {Tx_LastName} {Tx_SecondLastName}"; } }

        public bool Bo_Deleted { get { return this.Dt_Deleted.HasValue; } }

        [Required]
        [Display(Name = "Género")]
        public int Fk_Gender { get; set; }

        [Required]
        [Display(Name = "Tipo documento")]
        public int Fk_TypeDocumentIdentification { get; set; }

        [Required]
        [Display(Name = "N° Documento")]
        [StringLength(50, ErrorMessage = "Debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 2)]
        public string Tx_DescriptionDocumentIdentification { get; set; }

        public ICollection<PersonalInformationViewModel> PersonalInformation { get; set; }

        public virtual IList<CategoryViewModel> ListCategories { get; set; }

        public PersonViewModel()
        {
        }

        public PersonViewModel(Person person)
        {
            if (person == null)
            {
                return;
            }
            else
            {
                this.Bo_IsEmpty = false;
                this.Id_Person = person.ID_Person.ToString();
                this.Tx_FirtsName = person.TX_FirtsName;
                this.Tx_MiddleName = person.TX_MiddleName;
                this.Tx_LastName = person.TX_LastName;
                this.Tx_SecondLastName = person.TX_SecondLastName;
                this.Fk_Gender = person.FK_Gender;
                this.Dt_Birthday = person.DT_Birthday;
                this.Bo_Enabled = person.BO_Enabled;
                this.Bo_IsEmpty = false;
                this.PersonalInformation = person.PersonalInformation.Count() == 0 ? null : person.PersonalInformation.Select(x => new PersonalInformationViewModel(x)).ToList();
            }
        }

        public Person ConvertViewModelToClass() =>
            new Person
            {
                ID_Person = OwnHelper.ConvertStringToInt(this.Id_Person),
                TX_FirtsName = this.Tx_FirtsName,
                TX_MiddleName = this.Tx_MiddleName,
                TX_LastName = this.Tx_LastName,
                TX_SecondLastName = this.Tx_SecondLastName,
                FK_Gender = this.Fk_Gender,
                DT_Birthday = this.Dt_Birthday,
                DT_Deleted = this.Dt_Deleted,
                BO_Enabled = this.Bo_Enabled,
                PersonalInformation = this.PersonalInformation.IsNullOrEmpty() ? null : this.PersonalInformation.Select(x => x.ConvertViewModelToClass()).ToList()
            };

        public void GetActions() => this.Action = new Action(OwnHelper.ConvertStringToInt(this.Id_Person));


    }
}