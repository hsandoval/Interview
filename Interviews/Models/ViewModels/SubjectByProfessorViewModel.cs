using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Interviews.Models.Entities;

namespace Interviews.Models.ViewModels
{
    public class SubjectByProfessorViewModel : ViewModel<SubjectByProfessorViewModel>
    {
        public int Id_SubjectByProfessor { get; set; }

        [Required]
        public int Fk_Professor { get; set; }

        [Required]
        [Display(Name = "Area de conocimiento")]
        public int Fk_Subject { get; set; }

        [Display(Name = "Nombre profesor")]
        public string Tx_FullNameProfessor { get; set; }

        [Display(Name = "Area de conocimiento")]
        public string Tx_Subject { get; set; }

        public virtual ProfessorViewModel Professor { get; set; }

        public virtual TypeViewModel Subject { get; set; }

        public SubjectByProfessorViewModel() { }

        public SubjectByProfessorViewModel(SubjectByProfessor subject)
        {
            if (subject == null)
            {
                return;
            }
            Bo_IsEmpty = false;
            this.Id_SubjectByProfessor = subject.ID_SubjectByProfessor;
            this.Fk_Professor = subject.FK_Professor;
            this.Fk_Subject = subject.FK_Subject;
            this.Bo_Enabled = subject.BO_Enabled;
            this.Dt_Deleted = subject.DT_Deleted;
            if (subject.Professor != null) this.Tx_FullNameProfessor = $"{subject.Professor.Person.TX_FirtsName} {subject.Professor.Person.TX_MiddleName} {subject.Professor.Person.TX_LastName} {subject.Professor.Person.TX_SecondLastName}";
            if (subject.Subject != null) this.Tx_Subject = subject.Subject.TX_Description;
        }

        public SubjectByProfessor ConvertViewModelToClass() => new SubjectByProfessor
        {
            ID_SubjectByProfessor = this.Id_SubjectByProfessor,
            FK_Professor = this.Fk_Professor,
            FK_Subject = this.Fk_Subject,
            BO_Enabled = this.Bo_Enabled,
            DT_Deleted = this.Dt_Deleted,
        };

        public void GetActions() => this.Action = new Action(this.Id_SubjectByProfessor);
    }
}