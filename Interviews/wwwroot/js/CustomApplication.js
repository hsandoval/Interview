function MakeRequest(UrlAction, Data) {
	return new Promise(function (resolve, reject) {
	    UrlAction = `${window.location.protocol}//${window.location.host}/${UrlAction}`;

		const TOKEN = document.getElementsByName('__RequestVerificationToken')['0'].value;

		const headers = new Headers({
			'Authorization': `bearer ${TOKEN}`,
			'Content-Type': 'application/json; charset=utf-8'
		});

		fetch(UrlAction, {
			mode: 'cors',
			method: 'post',
			withCredentials: true,
			crossdomain: true,
			headers: headers,
			body: JSON.stringify(Data)
		})
			.then(response => response.text().then(data => ({ status: response.status, body: data })))
			.then((data) => resolve(data))
			.catch(err => reject(Error(err.message)));
	});
}

function MakeRequestGET(UrlAction, params) {
	return new Promise(function (resolve, reject) {
		UrlAction = new URL(`${window.location.protocol}//${window.location.host}/${UrlAction}`);
		Object.keys(params).forEach(key => UrlAction.searchParams.append(key, params[key]));

		fetch(UrlAction, {
			mode: 'cors',
			method: 'get',
			withCredentials: true,
			crossdomain: true,
		})
			.then(response => response.text().then(data => ({ status: response.status, body: data })))
			.then((data) => resolve(data))
			.catch(err => reject(Error(err.message)));
	});
}

function DeleteData(UrlAction, items) {
	let id;

	for (key in items) {
		if (key.includes('id_')) {
			id = items[key];
			break;
		}
	}

	return new Promise(function (resolve, reject) {

		fetch(`${UrlAction}?id=${id}`, {
			method: 'delete'
		})
			.then((resolved) => resolved.text())
			.then((data) => {
				resolve(data);
			})
			.catch(err => {
				reject(Error(err.message));
			});
	});
}

function IsNullOrEmpty(object) {
	if (typeof object === 'undefined') return true;
	if (typeof object === 'object') return true;
	if (object === null) return true;
	if (typeof object === '') return true;
	if (typeof object === ' ') return true;
	if (object === '') return true;
	if (object === ' ') return true;

	return false;
}


function GetFormData(idForm) {
	let jsonData = {};

	const elementsByContainer = GetElementsByContainer();

	$(`#${idForm} input, #${idForm} select `).each((index, item) => {
		let value;
		if (!IsNullOrEmpty(item.id)) {
			if (item.type === 'checkbox') value = `${item.checked}`;
			else {
				if (item.id.includes('Id_')) {
					value = IsNullOrEmpty(item.value) ? 0 : item.value;
				} else {
					value = item.value;
				}
			}
			jsonData[item['name']] = value;
		}
	});
	
	let data = Object.assign(jsonData, elementsByContainer);

	return data;
}

function GetElementsByContainer() {
	const dataContainer = $('[data-container-app]');
	return GroupElementsByContainer(dataContainer);
}

function GroupElementsByContainer(dataContainer) {
	let dataGroupByType = {};
	$.each(dataContainer, (index, item) => {
        let key = GetTypeData(item, 'containerApp');
		let value = GetDictionaryByItem(item);

		if (!(key.Class in dataGroupByType)) {
			if (key.Type = 'ICollection') {
				dataGroupByType[key.Class] = [];
				const iCollection = dataGroupByType[key.Class][key.Key];
				let newValue = typeof iCollection === 'undefined' ? value : Object.assign(dataGroupByType[key.Class][key.Key], value);
				dataGroupByType[key.Class][key.Key] = newValue;
			}
		} else {
			const iCollection = dataGroupByType[key.Class][key.Key];
			let newValue = typeof iCollection === 'undefined' ? value : Object.assign(dataGroupByType[key.Class][key.Key], value);
			dataGroupByType[key.Class][key.Key] = newValue;
		}
	});
	return dataGroupByType;
}

function GetTypeData(item, nameCustomData) {
	return $(`#${item.id}`).data(nameCustomData);
}

function GetDictionaryByItem(item) {
	let dict = {};
	let key = item.name;
	let value = $(`#${item.id}`).val();

	dict[key] = value;
	return dict;
}

function AnalizeDataForm(idForm) {
	let dataInContainer = $(`#${idForm}`).find('[data-container-app]').serializeArray();
	let allDataInForm = $(`#${idForm}`).serializeArray();

	let keysDataContainer = dataInContainer.map(a => a.name);

	return allDataInForm.filter(item => !(keysDataContainer.includes(item.name)));
}

let FormClear = (idForm) => {
	const $form = $(`#${idForm}`);

	$form.find('input').val('');
	$form.find('select option:selected').removeAttr('selected');
	$form.find('input:checkbox, input:radio').removeAttr('checked');
};

let IsTrue = (value) => {
	if (typeof (value) === 'string') {
		value = value.trim().toLowerCase();
	}
	switch (value) {
	case true:
	case 'true':
	case 1:
	case '1':
	case 'on':
	case 'yes':
		return true;
	default:
		return false;
	}
};

let HideElement = (idElement) => $(`#${idElement}`).addClass('d-none');

let UnhideElement = (idElement) => $(`#${idElement}`).removeClass('d-none');

let GetIconsFontAwesome = () => {
	const UrlAction = 'https://fontawesome.com/cheatsheet';

	fetch(UrlAction, {
		mode: 'no-cors',
		method: 'get',
		withCredentials: true,
		crossdomain: true,
	})
		.then(response => {
			response.text().then(
				data => ({ status: response.status, body: data }))
		})
		.catch(err => reject(Error(err.message)));
};

let fnCancelTimeChangesUTC = (date) => new Date(date.setHours(date.getHours() - date.getTimezoneOffset() / 60));
