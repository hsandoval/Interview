﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.ViewModels;

namespace Interviews.Models.Entities
{
    public class IconFontAwesome
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID_IconFontAwesome { get; set; }

        [Required]
        public string TX_Description { get; set; }

    }
}
