using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Caches;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class SurveyServiceController : Controller
    {

        public ILogger ILogger { get; set; }
        public SurveyServiceController(ILogger<SurveyServiceController> Logger)
        {
            ILogger = Logger;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "SurveyService";
            ViewBag.TextButton = "Enviar respuestas";
            ViewBag.FontAwesomeIcon = "fa fa-keyboard";
            ViewBag.Controller = "SurveyService";
            ViewBag.Parameter = "id_SurveyService";
            return View();
        }

        [HttpPost, ActionName("TryDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult TryDelete(int ID_Record)
        {
            SurveyServicesCache cache = new SurveyServicesCache();

            try
            {
                cache.Delete(ID_Record);
                return Json(ID_Record.ToString());
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult TryPopulateDataTable([FromBody] int id)
        {
            SurveyServicesCache cache = new SurveyServicesCache();
            try
            {
                SurveyServicesCache dataTable = cache.GetDataTableInSurveyServices();
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }
    }
}