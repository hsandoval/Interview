﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Entities
{
    public class UserApplication : IdentityUser
    {
        public int? FK_Person { get; set; }

        [ForeignKey("FK_Person")]
        public virtual Person Person { get; set; }
    }
}
