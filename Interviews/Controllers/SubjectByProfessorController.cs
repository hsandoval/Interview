using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Caches;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class SubjectByProfessorController : Controller
    {

        public ILogger ILogger { get; set; }
        public SubjectByProfessorController(ILogger<SubjectByProfessorController> Logger)
        {
            ILogger = Logger;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Areas de conocimiento por facilitador";
            ViewBag.Controller = "SubjectByProfessor";
            ViewBag.TextButton = " Agregar nueva area";
            ViewBag.FontAwesomeIcon = "far fa-address-book";
            ViewBag.Parameter = "id_SubjectByProfessor";
            return View();
        }

        [HttpPost]
        public IActionResult TryPopulateDataTable()
        {
            SubjectByProfessorCache cache = new SubjectByProfessorCache();
            try
            {
                SubjectByProfessorCache dataTable = cache.GetDataTable();
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        public IActionResult TryGetSubjectByProfessor(int id)
        {
            SubjectByProfessorCache cache = new SubjectByProfessorCache();

            try
            {
                SubjectByProfessorViewModel subject = cache.GetById(new SubjectByProfessorViewModel { Id_SubjectByProfessor = id });

                if (subject.IsNullOrEmpty())
                    return NotFound();
                else
                    return PartialView("EditorTemplate/SubjectByProfessorForm", subject);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult TrySaveChanges([FromBody] SubjectByProfessorViewModel viewModel)
        {
            SubjectByProfessorCache cache = new SubjectByProfessorCache();

            try
            {
                SubjectByProfessorViewModel subject = cache.TryEdit(viewModel);
                if (viewModel.Id_SubjectByProfessor != 0)
                    return Ok(subject);
                else
                    return Created("", subject);
            }
            catch (Exception ex)
            {
                string Message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost, ActionName("TryDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult TryDelete(int ID_Record)
        {
            SubjectByProfessorCache cache = new SubjectByProfessorCache();

            try
            {
                cache.Delete(ID_Record);
                return Json(ID_Record.ToString());
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }


        public IActionResult GetProfessorBySubject([FromBody] SubjectByProfessorViewModel viewModel)
        {
            SubjectByProfessorCache cache = new SubjectByProfessorCache();
            try
            {
                IList<ProfessorViewModel> data = cache.GetProfessorBySubject(viewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }
    }
}