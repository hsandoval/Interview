﻿using Interviews.Models.Entities;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Caches
{
    public class UserCache : Cache<UserViewModel>
    {
        readonly IUserRepository repository = new UserRepository();

        public UserCache GetDataTableInformation()
        {
            IList<ColumnDataTable> columns = GetColumnDataTable();
            IList<UserViewModel> users = SearchWith(x => !x.DT_Deleted.HasValue);
            users.ToList().ForEach(x => x.GetActions());
            return new UserCache()
            {
                ListColumnsDataTable = columns,
                ListData = users
            };
        }

        private IList<ColumnDataTable> GetColumnDataTable() =>
        new List<ColumnDataTable>() {
            new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_Person", Tx_ColumnTitle = "Id" },
            new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_FullName", Tx_ColumnTitle = "Nombre" },
            new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "dt_Birthday", Tx_ColumnTitle = "Fecha Nacimiento" },
            new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "nu_Age", Tx_ColumnTitle = "Edad" },
            new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
        };

        public IList<UserViewModel> SearchWith(Func<Person, bool> Predicate) =>
        repository.GetAllUsers(Predicate).Select(x => new UserViewModel(x)).ToList();

        public UserViewModel TryEdit(UserViewModel viewModel)
        {
            UserViewModel currentRecord = GetById(viewModel);
            UserViewModel returnedRecord = new UserViewModel();

            //if (currentRecord.IsNullOrEmpty())
            //    //returnedRecord = Insert(viewModel);
            //else
            //    //returnedRecord = Update(viewModel);

            return returnedRecord;
        }

        public UserViewModel GetById(UserViewModel viewModel)
        {
            Person person = new Person { ID_Person = OwnHelper.ConvertStringToInt(viewModel.Id_Person) };
            return new UserViewModel(repository.GetAllUsers(x => x.ID_Person == person.ID_Person).SingleOrDefault());
        }
        //UserViewModel Insert(UserViewModel viewModel)
        //{
        //    viewModel.Person.PersonalInformation = new List<PersonalInformationViewModel> {
        //            new PersonalInformationViewModel {
        //                Nu_PriorityIdentification = 1,
        //                Fk_TypeInformation = viewModel.Person.Fk_TypeDocumentIdentification,
        //                Tx_InformationDescription = viewModel.Person.Tx_DescriptionDocumentIdentification
        //            }
        //        };

        //    return new UserViewModel(repository.Insert(viewModel.ConvertViewModelProfessorToClass()));
        //}
        //}
    }
}
