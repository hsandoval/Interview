﻿using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Repositories {
		public interface IInterviewRepository : IRepository<Interview> {
				List<Interview> GetAllInterviews(Func<Interview, bool> predicate);
		}
		public class InterviewRepository : Repository<Interview>, IInterviewRepository {
				public List<Interview> GetAllInterviews(Func<Interview, bool> predicate) =>
						_EF.Interviews
						.Include(x => x.Topic)
						.Include(x => x.StateInterview)
						.Include(x => x.Professor)
						.ThenInclude(x => x.Person)
						.Include(x => x.Student)
						.ThenInclude(x => x.Person)
						.Where(predicate)
						.ToList();
		}
}
