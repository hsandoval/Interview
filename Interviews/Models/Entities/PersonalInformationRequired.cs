using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Interviews.Models.Entities {
		public class PersonalInformationRequired {
				[Key]
				[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
				public int ID_PersonalInformationRequired { get; set; }

				[Required]
				public string TX_Description { get; set; }

				[Required]
				public int FK_AssociatedCategory { get; set; }

				[Required]
				public bool BO_IsDropDownList { get; set; }
				
				[Required]
				public bool BO_Enabled { get; set; }

				public DateTime? DT_Deleted { get; set; }

				#region Virtual Properties

				[ForeignKey("FK_AssociatedCategory")]
				public virtual Category Category { get; set; }

				#endregion
		}
}