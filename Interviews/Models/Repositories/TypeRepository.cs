﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Interviews.Models.Repositories {
	public interface ITypeRepository : IRepository<Entities.Type> {
		List<Entities.Type> GetTypesByPredicate(Func<Entities.Type, bool> predicate = null);
	}

	public class TypeRepository : Repository<Entities.Type>, ITypeRepository {

		public List<Entities.Type> GetTypesByPredicate(Func<Entities.Type, bool> predicate = null) {
			var data = _EF.Types
				.Include(x => x.ChildrenTypes)
				.ThenInclude(x => x.IconFontAwesome)
				.Include(x => x.IconFontAwesome)
				.Where(predicate)
				.ToList();
			return data;
		}
	}
}