﻿using System.Linq;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Interviews.Models.Entities;

namespace Interviews.Models.Entities {
	public class ApplicationContext : IdentityDbContext<UserApplication> {
		public ApplicationContext(DbContextOptions<ApplicationContext> connectionString): base(connectionString) { }

		public virtual DbSet<Category> Categories { get; set; }
		public virtual DbSet<Type> Types { get; set; }
		public virtual DbSet<Person> Persons { get; set; }
		public virtual DbSet<PersonalInformation> PersonalInformation { get; set; }
		public virtual DbSet<PersonalInformationRequired> PersonalInformationRequired { get; set; }
		public virtual DbSet<Professor> Professors { get; set; }
		public virtual DbSet<Student> Students { get; set; }
		public virtual DbSet<SheduleByProfessor> SheduleByProfessors { get; set; }
		public virtual DbSet<SubjectByProfessor> SubjectsByProfessors { get; set; }
		public virtual DbSet<Interview> Interviews { get; set; }
        public virtual DbSet<SurveyService> SurveysServices { get; set; }
        public virtual DbSet<DetailSurveyService> DetailsSurveysServices { get; set; }
        public virtual DbSet<IconFontAwesome> IconsFontAwesome  { get; set; }
        public virtual DbSet<UserApplication> UsersApplication { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
			#region Unique Key

			modelBuilder.Entity<Category>()
				.HasIndex(x => new { x.TX_Description })
				.HasName("UQ_Categories_ON_TX_Description")
				.IsUnique();

			modelBuilder.Entity<Type>()
				.HasIndex(x => new { x.TX_Description, x.FK_ParentType, x.FK_Category })
				.HasName("UQ_Types_ON_TX_Description_And_FK_ParentType_And_FK_Category")
				.IsUnique();

			modelBuilder.Entity<PersonalInformationRequired>()
				.HasIndex(x => new { x.FK_AssociatedCategory, x.TX_Description })
				.HasName("UQ_PersonalInformationRequired_ON_FK_AssociatedCategory_And_TX_InformationDescription")
				.IsUnique();

			modelBuilder.Entity<Student>()
				.HasIndex(x => new { x.FK_Person })
				.HasName("UQ_Students_ON_FK_Person")
				.IsUnique();

			modelBuilder.Entity<Professor>()
				.HasIndex(x => new { x.FK_Person })
				.HasName("UQ_Professors_ON_FK_Person")
				.IsUnique();

			modelBuilder.Entity<SubjectByProfessor>()
				.HasIndex(x => new { x.FK_Professor, x.FK_Subject })
				.HasName("UQ_SubjectsByProfessors_ON_FK_Professor_And_FK_Subject")
				.IsUnique();

            modelBuilder.Entity<SurveyService>()
                .HasIndex(x => new { x.FK_Person, x.FK_Interview})
                .HasName("UQ_SurveyService_ON_FK_Person_And_FK_Interview")
                .IsUnique();

            modelBuilder.Entity<DetailSurveyService>()
                .HasIndex(x => new { x.FK_SurveyServices, x.FK_Ask})
                .HasName("UQ_DetailSurveyService_ON_FK_SurveyServices_And_FK_Ask")
                .IsUnique();

            modelBuilder.Entity<IconFontAwesome>()
                .HasIndex(x => new { x.TX_Description })
                .HasName("UQ_IconFontAwesome_ON_TX_Description")
                .IsUnique();
            #endregion

            #region Default

            modelBuilder.Entity<Category>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

			modelBuilder.Entity<Category>()
				.Property(x => x.BO_IsTypeFather)
				.HasDefaultValue(false);

			modelBuilder.Entity<Type>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

			modelBuilder.Entity<Person>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

			modelBuilder.Entity<PersonalInformation>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

			modelBuilder.Entity<PersonalInformationRequired>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

			modelBuilder.Entity<Student>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

			modelBuilder.Entity<Professor>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

			modelBuilder.Entity<SheduleByProfessor>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

			modelBuilder.Entity<SubjectByProfessor>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

			modelBuilder.Entity<Interview>()
				.Property(x => x.BO_Enabled)
				.HasDefaultValue(true);

            modelBuilder.Entity<Interview>()
                .Property(x => x.BO_Enabled)
                .HasDefaultValue(true);

            #endregion

            #region CancelDeleteCascade

            var ListCascadeFKs = modelBuilder.Model.GetEntityTypes()
				.SelectMany(t => t.GetForeignKeys())
				.Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

			foreach (var fk in ListCascadeFKs)
				fk.DeleteBehavior = DeleteBehavior.Restrict;

			base.OnModelCreating(modelBuilder);

			#endregion
		}
	}
}