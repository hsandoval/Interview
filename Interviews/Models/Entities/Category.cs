﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Entities {
	public class Category  {

		public Category() {
			Types = new HashSet<Type>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column(Order = 0)]
		public int ID_Category { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 1)]
		[Column(Order = 1)]
		public string TX_Description { get; set; }

        public int? FK_IconFontAwesome { get; set; }

        [Required]
		public bool BO_Enabled { get; set; }

		[Required]
		public bool BO_IsTypeFather { get; set; }

		public DateTime? DT_Deleted { get; set; }

		public virtual ICollection<Type> Types { get; set; }

        [ForeignKey("FK_IconFontAwesome")]
        public virtual IconFontAwesome IconFontAwesome { get; set; }
    }
}