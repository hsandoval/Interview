using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Caches;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class TypeController : Controller
    {

        public ILogger ILogger { get; set; }
        public TypeController(ILogger<TypeController> Logger)
        {
            ILogger = Logger;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Tipos";
            ViewBag.TextButton = " Agregar nueva entidad";
            ViewBag.FontAwesomeIcon = "far fa-address-book";
            ViewBag.Controller = "Type";
            ViewBag.Parameter = "id_Type";
            return View();
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public IActionResult TryEdit([FromBody] TypeViewModel ViewModel)
        {
            TypeCache cache = new TypeCache();

            try
            {
                TypeViewModel Category = cache.TryEdit(ViewModel);
                return Created("", Category);
            }
            catch (Exception ex)
            {
                string Message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost, ActionName("TryDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult TryDelete(int ID_Record)
        {
            TypeCache cache = new TypeCache();

            try
            {
                cache.Delete(ID_Record);
                return Json(ID_Record.ToString());
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult GetTypesChildrenByFather([FromBody] TypeViewModel viewModel)
        {
            TypeCache cache = new TypeCache();

            try {
                TypeViewModel type = cache.GetTypeWithChildrenTypes(viewModel);

            if (type.ChildrenTypes.IsNullOrEmpty())
                return NotFound();
            else
                return Ok(type.ChildrenTypes);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }         
    }
}