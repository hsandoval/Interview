using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Interviews.Models.Entities
{
    public class Person
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID_Person { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string TX_FirtsName { get; set; }

        [StringLength(50, MinimumLength = 2)]
        public string TX_MiddleName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string TX_LastName { get; set; }

        [StringLength(50, MinimumLength = 2)]
        public string TX_SecondLastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime DT_Birthday { get; set; }

        [Required]
        public int FK_Gender { get; set; }

        [Required]
        public bool BO_Enabled { get; set; }

        public DateTime? DT_Deleted { get; set; }

        public virtual ICollection<PersonalInformation> PersonalInformation { get; set; }

        [ForeignKey("FK_Gender")]
        public virtual Entities.Type Gender { get; set; }

        public virtual UserApplication UserApplication { get; set; }

        public virtual Professor Professors { get; set; }

        public virtual Student Students { get; set; }
    }
}