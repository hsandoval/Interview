﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Entities
{
    public class SurveyService
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID_SurveyService { get; set; }

        [Required]
        public int FK_Interview { get; set; }

        [Required]
        public int FK_Person { get; set; }

        [Required]
        public DateTime DT_Created { get; set; }

        [ForeignKey("FK_Interview")]
        public virtual Interview Interview { get; set; }

        [ForeignKey("FK_Person")]
        public virtual Person Person { get; set; }

    }
}
