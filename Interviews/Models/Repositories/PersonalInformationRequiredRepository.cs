﻿using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Repositories {
	public interface IPersonalInformationRequiredRepository : IRepository<PersonalInformationRequired>
	{
		IList<PersonalInformationRequired> SearchWithCategory(Func<PersonalInformationRequired, bool> predicate);
	}
	public class PersonalInformationRequiredRepository : Repository<PersonalInformationRequired>, IPersonalInformationRequiredRepository { 
		public IList<PersonalInformationRequired> SearchWithCategory(Func<PersonalInformationRequired, bool> predicate) =>
			_EF.PersonalInformationRequired
			.Include(x => x.Category)
			.ToList();
	}
}
