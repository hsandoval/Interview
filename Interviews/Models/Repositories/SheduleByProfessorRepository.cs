﻿using Interviews.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Repositories
{
    public interface ISheduleByProfessorRepository : IRepository<SheduleByProfessor> {
        IList<SheduleByProfessor> GetSheduleByProfessor(SheduleByProfessor sheduleByProfessor);
    }
    public class SheduleByProfessorRepository : Repository<SheduleByProfessor>, ISheduleByProfessorRepository
    {
        public IList<SheduleByProfessor> GetSheduleByProfessor(SheduleByProfessor sheduleByProfessor) =>
            SearchWith(x =>
                x.FK_Professor == sheduleByProfessor.FK_Professor &&
                x.BO_Enabled &&
                !x.DT_Deleted.HasValue
            ).ToList();
    }
}
