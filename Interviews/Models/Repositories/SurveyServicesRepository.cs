using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Repositories {
	public interface ISurveyServiceRepository : IRepository<SurveyService>
	{
		IList<SurveyService> GetAllWithMultipleRelationedInformation(Func<SurveyService, bool> predicate = null);
	}
	public class SurveyServiceRepository : Repository<SurveyService>, ISurveyServiceRepository
	{
		public IList<SurveyService> GetAllWithMultipleRelationedInformation(Func<SurveyService, bool> predicate = null) =>
			_EF.SurveysServices
			.Include(x => x.Interview)
			.ThenInclude(x => x.Student)
			.ThenInclude(x => x.Person)
			.Include(x => x.Interview)
			.ThenInclude(x => x.Professor)
			.ThenInclude(x => x.Person)
			.Include(x => x.Interview)
			.ThenInclude(x => x.Topic)
			.Where(predicate)
			.ToList();
	}
}
