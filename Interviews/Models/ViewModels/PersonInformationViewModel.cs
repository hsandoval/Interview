﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;

namespace Interviews.Models.ViewModels
{
    public class PersonalInformationViewModel : ViewModel<PersonalInformationViewModel>
    {
        public int Id_PersonalInformation { get; set; }

        public string Tx_InformationDescription { get; set; }

        [Required]
        public int Fk_TypeInformation { get; set; }
        public string Tx_DescriptionTypeInformation { get; }
        public string Tx_DescriptionCategoryInformation { get; }

        public int? Nu_PriorityIdentification { get; set; }

        [Required]
        public int Fk_Person { get; set; }

        public string Tx_FullNamePerson { get; }

        public bool Bo_Deleted { get { return this.Dt_Deleted.HasValue; } }

        public virtual CategoryViewModel TypeInformation { get; set; }

        public virtual PersonViewModel Person { get; set; }

        public PersonalInformationViewModel() { }

        public PersonalInformationViewModel(PersonalInformation personalInformation)
        {
            if (personalInformation == null)
            {
                return;
            }
            this.Bo_IsEmpty = false;
            this.Id_PersonalInformation = personalInformation.ID_PersonalInformation;
            this.Tx_InformationDescription = personalInformation.TX_InformationDescription;
            this.Bo_Enabled = personalInformation.BO_Enabled;
            this.Fk_Person = personalInformation.FK_Person;
            this.Fk_TypeInformation = personalInformation.FK_Type;
            this.Nu_PriorityIdentification = personalInformation.NU_PriorityIdentification;
            this.Tx_FullNamePerson = $"{personalInformation.Person.TX_FirtsName} {personalInformation.Person.TX_MiddleName} {personalInformation.Person.TX_LastName} {personalInformation.Person.TX_SecondLastName}";
            if (personalInformation.Type != null)
            {
                this.Tx_DescriptionTypeInformation = personalInformation.Type.TX_Description;
                this.Tx_DescriptionCategoryInformation = personalInformation.Type.Category.TX_Description;
            }
        }

        public PersonalInformation ConvertViewModelToClass() => new PersonalInformation
        {
            ID_PersonalInformation = this.Id_PersonalInformation,
            FK_Type = this.Fk_TypeInformation,
            FK_Person = this.Fk_Person,
            TX_InformationDescription = this.Tx_InformationDescription,
            NU_PriorityIdentification = this.Nu_PriorityIdentification,
            BO_Enabled = this.Bo_Enabled,
            DT_Deleted = this.Dt_Deleted
        };

        public void GetActions() => this.Action = new Action(this.Id_PersonalInformation);
    }
}