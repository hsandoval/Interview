﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Interviews.Models.Repositories {
	public interface ICategoryRepository : IRepository<Category> {
		List<Category> GetCategoriesByPredicate(Func<Category, bool> predicate);
		List<Category> GetAllCategories();
	}

	public class CategoryRepository : Repository<Category>, ICategoryRepository {

		public List<Category> GetCategoriesByPredicate(Func<Category, bool> predicate) {
			var data = _EF.Categories
				.Include(x => x.Types)
				.ThenInclude(t => t.ChildrenTypes)
				.Where(predicate)
				.ToList();
			return data;
		}

		public List<Category> GetAllCategories() {
			var data = _EF.Categories
				.Include(x => x.Types)
				.ThenInclude(t => t.ChildrenTypes)
				.ToList();
			return data;
		}
	}
}