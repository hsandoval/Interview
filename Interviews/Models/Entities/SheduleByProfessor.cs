using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Interviews.Models.Entities {
		public class SheduleByProfessor {
				[Key]
				[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
				public int ID_SheduleByProfessor { get; set; }

				[Required]
				public int FK_Professor { get; set; }

				[Required]
				public DateTime DT_From { get; set; }

				[Required]
				public DateTime DT_To { get; set; }

				[Required]
				public bool BO_Enabled { get; set; }

				public DateTime? DT_Deleted { get; set; }

				[ForeignKey("FK_Professor")]
				public virtual Professor Professor { get; set; } 
		}
}