﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Caches;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class SheduleByProfessorController : Controller
    {

        public ILogger ILogger { get; set; }
        public SheduleByProfessorController(ILogger<SheduleByProfessorController> Logger)
        {
            ILogger = Logger;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Fechas disponibles para brindar asesorias";
            ViewBag.TextButton = " Agregar disponibilidad";
            ViewBag.FontAwesomeIcon = "fas fa-graduation-cap";
            ViewBag.Controller = "SheduleByProfessor";
            ViewBag.Parameter = "id_SheduleByProfessor";
            return View();
        }


        [HttpPost]
        // [ValidateAntiForgeryToken]
        public IActionResult TrySaveChanges([FromBody] FullCalendarViewModel viewModel)
        {
            SheduleByProfessorCache cache = new SheduleByProfessorCache();

            try
            {
                SheduleByProfessorViewModel shedule = cache.TryEdit(viewModel.ConvertToSheduleByProfessorViewModel());
                if (viewModel.Id.IsNullOrEmpty())
                    return Ok(shedule);
                else
                    return Created("", shedule);
            }
            catch (Exception ex)
            {
                string Message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost, ActionName("TryDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult TryDelete(int ID_Record)
        {
            SheduleByProfessorCache cache = new SheduleByProfessorCache();

            try
            {
                cache.Delete(ID_Record);
                return Json(ID_Record.ToString());
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult TryPopulateDataTable([FromBody] int id)
        {
            SheduleByProfessorCache cache = new SheduleByProfessorCache();
            try
            {
                SheduleByProfessorCache dataTable = cache.GetDataTableInformationByIdProfessor(new SheduleByProfessorViewModel { Fk_Professor = id });
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }
    }
}