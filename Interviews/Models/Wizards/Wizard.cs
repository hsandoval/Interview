﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Interviews.Models.Wizards
{
    public class ReflectionOwn
    {

        string FullNameSpaceClass { get; set; }
        Object[] ParametersConstructorClass { get; set; }
        object Obj { get; set; }

        public ReflectionOwn(string _FullNameSpaceClass, Object[] _ParametersConstructorClass)
        {
            FullNameSpaceClass = _FullNameSpaceClass;
            ParametersConstructorClass = _ParametersConstructorClass;
            Obj = Activator.CreateInstance(GetTypeObject(), ParametersConstructorClass);
        }

        System.Type GetTypeObject() => System.Type.GetType(FullNameSpaceClass);

        public string GetPropertyValue(string NameProperty)
        {
            var property = Obj.GetType().GetProperty(NameProperty);
            if (property == null)
                return string.Empty;
            return Obj.GetType().GetProperty(NameProperty).GetValue(Obj, null).ToString();
        }

        public string GetPropertyValueInObject(string NameObject, string NameProperty)
        {
            var _obj = Obj.GetType().GetProperty(NameObject).GetValue(Obj, null);
            var property = _obj.GetType().GetProperty(NameProperty);
            if (property == null)
                return string.Empty;
            return _obj.GetType().GetProperty(NameProperty).GetValue(_obj, null).ToString();
        }

        public ListDictionary GetPropertyCollection(string NameProperty)
        {
            ListDictionary ListObj = new ListDictionary();

            var myList = Obj.GetType().GetProperty(NameProperty).GetValue(Obj, null);
            if (myList.GetType().IsGenericType)
            {
                string TypeName = myList.GetType().GetGenericTypeDefinition().Name;
                if (TypeName == "List`1")
                {
                    int n = (int)myList.GetType().GetProperty("Count").GetValue(myList, null);

                    for (int i = 0; i < n; i++)
                    {
                        object[] index = { i };
                        object myObject = myList.GetType().GetProperty("Item").GetValue(myList, index);

                        PropertyInfo[] objectProperties = myObject.GetType().GetProperties();

                        Dictionary<string, string> obj = new Dictionary<string, string>();
                        foreach (PropertyInfo currentProperty in objectProperties)
                        {
                            obj.Add(currentProperty.Name, currentProperty.GetValue(myObject, null).ToString());
                        }
                        ListObj.Add(i, obj);
                    }
                }
            }

            return ListObj;
        }
    }

    public static class IEnumerableExtension
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            if (source == null)
                return true;

            var data = (IList<T>)source;

            if (data.Count == 0)
                return true;

            return false;

            //return dbContext.GL00100.Join(dbContext.GL00105.Where(x => x.ACTNUMST == id),
            //														l => l.ACTINDX,
            //														c => c.ACTINDX,
            //														(l, c) => l).FirstOrDefault();
        }
    }

    public static class OwnHelper
    {
        public static int ConvertStringToInt(string value)
        {
            Int32.TryParse(value, out int newValue);
            return newValue;
        }

        public static bool TimeBetween(DateTime datetimeCompare, TimeSpan start, TimeSpan end)
        {
            TimeSpan now = datetimeCompare.TimeOfDay;
            if (start < end)
                return start <= now && now <= end;
            return !(end < now && now < start);
        }

        public static async Task ForEachAsync<T>(this IEnumerable<T> enumerable, Func<T, Task> action)
        {
            foreach (var item in enumerable)
            {
                await action(item);
            }
        }
    }
}