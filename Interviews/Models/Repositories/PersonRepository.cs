﻿using Interviews.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Repositories {
	public interface IPersonRepository : IRepository<Person> { }
	public class PersonRepository : Repository<Person>, IPersonRepository { }
}
