function CreateJSTreeEdit(IdTagHtml, NameController, MethodSelect, MethodInsert, MethodEdit, MethodDelete, MainTypeId) {
	const Controller = NameController;
	var obj = {
		'Id_Category': MainTypeId,
	};

	let HttpRequest = MakeRequest(`${Controller}/${MethodSelect}`, obj);

	HttpRequest
      .then(res => {
			GenerateJSTree(IdTagHtml, NameController, MethodInsert, MethodEdit, MethodDelete, $.parseJSON(res.body));
		}).catch((err) => {
			console.log(`Algo salió mal: ${err}`);
		});
}

function GenerateJSTree(IdTagHtml, NameController, MethodInsert, MethodEdit, MethodDelete, data) {
	const Controller = NameController;
	$(`#${IdTagHtml}`).jstree('destroy');
	
	$(`#${IdTagHtml}`).jstree({
		'core': {
			'check_callback': true,
			'data': data,
			'themes': {
				'responsive': true
			}
        },
        'types': {
            'default': {
                'icon': 'fa fa-folder text-warning fa-lg'
            },
            'file': {
                'icon': 'fa fa-file text-inverse fa-lg'
            }
        },
		'plugins': ['wholerow', 'checkbox'],
		'checkbox': {
			'keep_selected_style': false,
			'whole_node': false,
			'tie_selection': false,
			'three_state': false,
			'cascade': 'undetermined'
		}
	}).on('rename_node.jstree', function (e, data) {
		var obj = {
			'Id_Category': data.node.id,
			'Fk_ParentCategory': data.node.parent,
			'Tx_Description': data.node.text,
		};

		let HttpRequest = MakeRequest(`${Controller}/${MethodEdit}`, obj);

		HttpRequest
			.then(res => {
				data.instance.set_id(data.node, res.id_Category);
				data.instance.refresh();
			}).catch((err) => {
				console.log(`Algo salió mal: ${err}`);
				data.instance.refresh();
			});
	}).on('move_node.jstree', function (e, data) {
		$.get('?operation=move_node', { 'id': data.node.id, 'parent': data.parent, 'position': data.position })
			.fail(function () {
				data.instance.refresh();
			});
	}).on('copy_node.jstree', function (e, data) {
		$.get('?operation=copy_node', { 'id': data.original.id, 'parent': data.parent, 'position': data.position })
			.always(function () {
				data.instance.refresh();
			});
	}).on('changed.jstree', function (e, data) {
		if (data && data.selected && data.selected.length) {
			$.get('?operation=get_content&id=' + data.selected.join(':'), function (d) {
				$('#data .default').text(d.content).show();
			});
		} else {
			$('#data .content').hide();
			$('#data .default').text('Select a file from the tree.').show();
		}
	}).on('check_node.jstree uncheck_node.jstree', function(e, data) {
		if (data.node.state.checked ) {
			$('button[name="btnEditTreeView"]').removeClass('d-none');
		} else {
			$('button[name="btnEditTreeView"]').addClass('d-none');
		}
	});
}

let GetNodesChecked = (idTreView) => {
	let checked = [];

	const data = $(`#${idTreView} a.jstree-checked`);

	data.filter((x,d) => {
		checked.push(d.id.replace('_anchor',''));
	});

	return checked;
}