﻿using System;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Interviews.Models {

	public class AplicationConnection {

		static AplicationConnection instance = null;
		public IServiceCollection _services;
		public DbContextOptions<ApplicationContext> DbContextOptions;
		public string StringConnection;

		private AplicationConnection(TypeDatabaseEnum typeDatabase, string connectionString, IServiceCollection services = null) {

			StringConnection = connectionString;

			switch (typeDatabase) {
				case TypeDatabaseEnum.SqlServer:
					services.AddEntityFrameworkSqlServer().AddDbContext<ApplicationContext>(
						option => option.UseSqlServer(connectionString)
					);
					_services = services;
					DbContextOptions = new DbContextOptionsBuilder<ApplicationContext>()
						.UseSqlServer(connectionString)
						.Options;
					break;
				case TypeDatabaseEnum.Postgress:
					services.AddEntityFrameworkNpgsql().AddDbContext<ApplicationContext>(
						option => option.UseNpgsql(connectionString)
					);
					DbContextOptions = new DbContextOptionsBuilder<ApplicationContext>()
						.UseNpgsql(connectionString)
						.Options;
					break;
				case TypeDatabaseEnum.MySql:
					services.AddEntityFrameworkMySql().AddDbContext<ApplicationContext>(
						option => option.UseMySql(connectionString)
					);
					DbContextOptions = new DbContextOptionsBuilder<ApplicationContext>()
						.UseMySql(connectionString)
						.Options;
					break;
				case TypeDatabaseEnum.SqlLite:
					services.AddEntityFrameworkSqlite().AddDbContext<ApplicationContext>(
						option => option.UseSqlite(connectionString)
					);
					DbContextOptions = new DbContextOptionsBuilder<ApplicationContext>()
						.UseSqlite(connectionString)
						.Options;
					break;
			}
		}

		public static AplicationConnection Instance(IServiceCollection services = null) {
			if (instance == null)
				instance = new AplicationConnection(TypeDatabaseEnum.SqlServer, ConnectionStringEnum.HenkWindowsPC, services);

			return instance;
		}
	}
}