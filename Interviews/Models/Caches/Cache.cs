using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

public class Cache<T> {
	public IList<T> ListData { get; set; }
	public T DataOnly { get; set; }
	public IList<ColumnDataTable> ListColumnsDataTable { get; set; }

}
public class ColumnDataTable {
	public bool Bo_Hidden { get; set; }
	public string Tx_ColumnName { get; set; }
	public string Tx_ColumnTitle { get; set; }
}