﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Interviews.Models.Entities;

namespace Interviews.Models.ViewModels
{
    public class CategoryViewModel : ViewModel<CategoryViewModel>
    {
        [Display(Name = "Id categoria")]
        public int Id_Category { get; set; }

        [Display(Name = "Descripción")]
        [StringLength(50, ErrorMessage = "El nombre de la categoria debe ser de mínimo dos (2) y máximo cincuenta (50) caracteres.", MinimumLength = 2)]
        [Required(ErrorMessage = "El nombre de la categoria es requerido")]
        public string Tx_Description { get; set; }

        [Display(Name = "Registro padre")]
        public bool Bo_IsTypeFather { get; set; }

        [Display(Name = "Eliminado")]
        public bool Bo_Deleted { get { return this.Dt_Deleted.HasValue; } }

        bool Bo_IncludeRegistersDeleted { get; set; }

        [Display(Name = "Icono")]
        public int Fk_IconFontAwesome { get; set; }

        public virtual IconFontAwesomeViewModel IconFontAwesome { get; set; }

        public virtual TypeViewModel Type { get; set; }

        public virtual IList<CategoryViewModel> Categories { get; set; }

        public virtual IList<TypeViewModel> ChildrenTypes { get; set; }

        public CategoryViewModel() { }

        public CategoryViewModel(Category category)
        {
            if (category == null)
            {
                return;
            }
            else
            {
                Bo_IsEmpty = false;
                Id_Category = category.ID_Category;
                Tx_Description = category.TX_Description;
                Bo_Enabled = category.BO_Enabled;
                Dt_Deleted = category.DT_Deleted;
                Bo_IsTypeFather = category.BO_IsTypeFather;
                if (category.Types.Any())
                {
                    ChildrenTypes = new List<TypeViewModel>();
                    category.Types.ToList().ForEach(x => ChildrenTypes.Add(new TypeViewModel(x)));
                }

                if (category.IconFontAwesome != null)
                Fk_IconFontAwesome = category.IconFontAwesome.ID_IconFontAwesome;
            }
        }

        public Category ConvertViewModelToClass()
        {
            return new Category
            {
                ID_Category = this.Id_Category,
                TX_Description = this.Tx_Description,
                BO_Enabled = this.Bo_Enabled,
                DT_Deleted = this.Dt_Deleted,
                BO_IsTypeFather = this.Bo_IsTypeFather,
                FK_IconFontAwesome = this.IconFontAwesome.Id_IconFontAwesome
            };
        }

        public void GetActions() => this.Action = new Action(this.Id_Category);
    }
}