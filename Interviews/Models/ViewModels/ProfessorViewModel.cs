using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Interviews.Models.Entities;
using Interviews.Models.Wizards;
using MoreLinq;

namespace Interviews.Models.ViewModels
{
    public class ProfessorViewModel : PersonViewModel
    {
        public int Id_Professor { get; set; }

        [Required]
        public int Fk_Person { get; set; }

        [Required]
        public bool Bo_EnabledProf { get; set; }

        public DateTime? Dt_DeletedProf { get; set; }

        public virtual PersonViewModel Person { get; set; }

        public SheduleByProfessorViewModel Shedule { get; set; }

        public ProfessorViewModel() { }

        public ProfessorViewModel(Professor professor)
        {
            if (professor == null)
            {
                return;
            }
            else
            {
                this.Bo_IsEmpty = false;
                this.Id_Professor = professor.ID_Professor;
                this.Fk_Person = professor.FK_Person;
                this.Bo_EnabledProf = professor.BO_Enabled;
                this.Dt_DeletedProf = professor.DT_Deleted;
                if (professor.Person != null)
                {
                    this.Id_Person = professor.Person.ID_Person.ToString();
                    this.Tx_FirtsName = professor.Person.TX_FirtsName;
                    this.Tx_MiddleName = professor.Person.TX_MiddleName;
                    this.Tx_LastName = professor.Person.TX_LastName;
                    this.Tx_SecondLastName = professor.Person.TX_SecondLastName;
                    this.Fk_Gender = professor.Person.FK_Gender;
                    this.Dt_Birthday = professor.Person.DT_Birthday;
                    this.Bo_Enabled = professor.Person.BO_Enabled;
                    if (professor.Person.PersonalInformation != null)
                    {
                        PersonalInformation MainPersonalInformation = professor.Person.PersonalInformation.MinBy(x => x.NU_PriorityIdentification);
                        this.Fk_TypeDocumentIdentification = MainPersonalInformation.FK_Type;
                        this.Tx_DescriptionDocumentIdentification = MainPersonalInformation.TX_InformationDescription;
                        this.PersonalInformation = professor.Person.PersonalInformation.Count() == 0 ? null : professor.Person.PersonalInformation.Select(x => new PersonalInformationViewModel(x)).ToList();
                    }
                }
            }

        }
        public Professor ConvertViewModelProfessorToClass() => new Professor
        {
            ID_Professor = this.Id_Professor,
            FK_Person = this.Fk_Person,
            BO_Enabled = this.Bo_EnabledProf,
            DT_Deleted = this.Dt_DeletedProf,
            Person = this.Person.ConvertViewModelToClass()
        };

        public void GetActionsProfessor() => this.Action = new Action(this.Id_Professor);
    }
}