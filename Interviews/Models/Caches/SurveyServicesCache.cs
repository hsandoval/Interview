using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;

namespace Interviews.Models.Caches
{
    public class SurveyServicesCache : Cache<SurveyServiceViewModel>
    {
        readonly ISurveyServiceRepository repository = new SurveyServiceRepository();

        public SurveyServicesCache GetDataTableInSurveyServices()
        {
            IList<ColumnDataTable> columns = GetColumnDataTable();
            IList<SurveyServiceViewModel> surveyServices = SearchWith().ToList();
            surveyServices.ToList().ForEach(x => x.GetActions());
            return new SurveyServicesCache()
            {
                ListColumnsDataTable = columns,
                ListData = surveyServices
            };
        }

		private IList<SurveyServiceViewModel> SearchWith(Func<SurveyService, bool> predicate = null) =>
            repository.GetAllWithMultipleRelationedInformation(predicate).Select(x => new SurveyServiceViewModel(x)).ToList();

		private IList<ColumnDataTable> GetColumnDataTable() =>
        new List<ColumnDataTable>() {
                    new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_SurveyService", Tx_ColumnTitle = "Id" },
                    new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_Interview", Tx_ColumnTitle = "Entrevista" },
                    new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_Person", Tx_ColumnTitle = "Persona" },
                    new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "tx_FullNamePerson", Tx_ColumnTitle = "Persona" },
                    new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "tx_FullNameProfessor", Tx_ColumnTitle = "Persona" },
                    new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_Person", Tx_ColumnTitle = "Persona" },
                    new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "dt_Created", Tx_ColumnTitle = "Fecha de realización" },
					
        };

        internal void Delete(int iD_Record)
        {
            throw new NotImplementedException();
        }

        public IList<SurveyServiceViewModel> GetAll() =>
            repository.GetAll().Select(x => new SurveyServiceViewModel(x)).ToList();
    }
}