﻿using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Repositories {
		public interface IPersonalInformationRepository : IRepository<PersonalInformation> {
				List<PersonalInformation> SearchWithTypeAndPerson(Func<PersonalInformation, bool> predicate);
		}
		public class PersonalInformationRepository : Repository<PersonalInformation>, IPersonalInformationRepository {
				public List<PersonalInformation> SearchWithTypeAndPerson(Func<PersonalInformation, bool> predicate) =>
						_EF.PersonalInformation
						.Include(x => x.Type)
						.ThenInclude(x => x.Category)
						.Include(x => x.Person)
						.Where(predicate)
						.ToList();
		}
}
