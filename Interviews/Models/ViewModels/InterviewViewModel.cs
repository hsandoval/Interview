using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Interviews.Models.Entities;
using Interviews.Models.Wizards;

namespace Interviews.Models.ViewModels
{
    public class InterviewViewModel : ViewModel<InterviewViewModel>
    {
        public int Id_Interview { get; set; }

        [Required]
        [Display(Name = "Profesor")]
        public int Fk_Professor { get; set; }

        public string Tx_FullNameProfessor { get; set; }

        [Required]
        [Display(Name = "Estudiante")]
        public int Fk_Student { get; set; }

        public string Tx_FullNameStudent { get; set; }

        [Required]
        [Display(Name = "Tipicación")]
        public int Fk_Topic { get; set; }

        public string Tx_Topic { get; set; }

        [Required]
        [Display(Name = "Estatus")]
        public int Fk_StateInterview { get; set; }

        public string Tx_StateInterview { get; set; }

        [Required]
        [Display(Name = "Fecha entrevista")]
        public DateTime Dt_Interview { get; set; }

        [Required]
        [Display(Name = "Motivo solicitud entrevista")]
        [StringLength(500, ErrorMessage = "Debe tener entre {2} y {1} caracteres de longitud.", MinimumLength = 50)]
        public string Tx_DescriptionInterview { get; set; }

        public string Tx_UrlInterview { get; set; }

        public string Tx_TagHtmlRedirect { get; set; }

        public virtual ProfessorViewModel Professor { get; set; }

        public virtual StudentViewModel Student { get; set; }

        public virtual TypeViewModel Topic { get; set; }

        public virtual TypeViewModel StateInterview { get; set; }

        public InterviewViewModel() { }

        public InterviewViewModel(Interview interview)
        {
            if (interview == null)
            {
                return;
            }
            this.Bo_IsEmpty = false;
            this.Id_Interview = interview.ID_Interview;
            this.Fk_Professor = interview.FK_Professor;
            this.Fk_Student = interview.FK_Student;
            this.Fk_Topic = interview.FK_Topic;
            this.Fk_StateInterview = interview.FK_StateInterview;
            this.Tx_DescriptionInterview = interview.TX_DescriptionInterview;
            this.Tx_UrlInterview = interview.TX_UrlInterview;
            this.Bo_Enabled = interview.BO_Enabled;
            this.Dt_Interview = interview.DT_Interview;
            this.Dt_Deleted = interview.DT_Deleted;
            if (interview.Professor != null)
                this.Tx_FullNameProfessor = $"{interview.Professor.Person.TX_FirtsName} {interview.Professor.Person.TX_MiddleName} {interview.Professor.Person.TX_LastName} {interview.Professor.Person.TX_SecondLastName}";
            if (interview.Student != null)
                this.Tx_FullNameStudent = $"{interview.Student.Person.TX_FirtsName} {interview.Student.Person.TX_MiddleName} {interview.Student.Person.TX_LastName} {interview.Student.Person.TX_SecondLastName}";
            if (interview.Topic != null) this.Tx_Topic = interview.Topic.TX_Description;
            if (interview.StateInterview != null) this.Tx_StateInterview = interview.StateInterview.TX_Description;
        }

        public Interview ConvertViewModelToClass() => new Interview
        {
            ID_Interview = this.Id_Interview,
            FK_Professor = this.Fk_Professor,
            FK_Student = this.Fk_Student,
            FK_Topic = this.Fk_Topic,
            FK_StateInterview = this.Fk_StateInterview,
            TX_DescriptionInterview = this.Tx_DescriptionInterview,
            TX_UrlInterview = this.Tx_UrlInterview,
            DT_Interview = this.Dt_Interview,
            BO_Enabled = this.Bo_Enabled,
            DT_Deleted = this.Dt_Deleted,
        };

        public void GetActions() => this.Action = new Action(this.Id_Interview);

        public void SetStateRedirectInterview()
        {
            if (this.Dt_Interview >= DateTime.Now.AddMinutes(-15) && this.Dt_Interview <= DateTime.Now.AddMinutes(30))
                this.Tx_TagHtmlRedirect = $"<a target='_blank' href='{this.Tx_UrlInterview}' class='btn btn-sm btn-primary'>Entrar</a>";
            else this.Tx_TagHtmlRedirect = $"<a class='btn btn-sm btn-grey disabled'>Entrar</a>";
        }
    }
}