using System;
using Interviews.Models.Caches;
using Interviews.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers {
		public class InterviewController : Controller {

		public ILogger ILogger { get; set; }
		public InterviewController(ILogger<InterviewController> Logger) {
			ILogger = Logger;
		}

		public ActionResult Index() {
			ViewBag.Title = "Entrevistas";
			ViewBag.TextButton = "Solicitar nueva entrevista";
            ViewBag.FontAwesomeIcon = "fa fa-plus";
            ViewBag.Controller = "Interview";
            ViewBag.Parameter = "id_Interview";
			return View();
		}

        public IActionResult Create()
        {
            ViewBag.Title = "Solicitar nueva asesoria";
           return View("InterviewForm");
        }

        [HttpPost]
		public ActionResult TryPopulateDataTable() {
			try {
				InterviewCache cache = new InterviewCache();
				InterviewCache dataTable = cache.GetDataTableInformation();
				return Ok(dataTable);
			} catch (Exception ex) {
				string Message = $"Error al obtener los registros: {ex}";
				ILogger.LogError(Message);
				return BadRequest(Message);
			}
		}

        [HttpPost]
        public ActionResult TryPopulateDataTableByStudent([FromBody] int Id)
        {
            try
            {
                InterviewCache cache = new InterviewCache();
                InterviewCache dataTable = cache.GetInterviewsByStudent(new InterviewViewModel { Fk_Student = Id });
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost]
		// [ValidateAntiForgeryToken]
		public IActionResult TrySaveChanges([FromBody] InterviewViewModel viewModel) {
			InterviewCache cache = new InterviewCache();

			try {
				InterviewViewModel interview = cache.TryEdit(viewModel);
				return Created("", interview);
			} catch (Exception ex) {
				string Message = $"Error al insertar un nuevo registro: error -> {ex}";
				ILogger.LogError(Message);
				return BadRequest(Message);
			}
		}

		[HttpPost, ActionName("TryDelete")]
		[ValidateAntiForgeryToken]
		public ActionResult TryDelete(int ID_Record) {
			InterviewCache cache = new InterviewCache();

			try {
				cache.Delete(ID_Record);
				return Json(ID_Record.ToString());
			} catch (Exception ex) {
				return Json(new { success = false, error = ex.Message });
			}
		}
	}
}