﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;

namespace Interviews.Models.Caches {
	public class PersonalInformationCache : Cache<PersonalInformationViewModel> {
		readonly IPersonalInformationRepository repository = new PersonalInformationRepository();

		public PersonalInformationCache GetDataTableInformationByPerson(PersonViewModel person) {
			IList<ColumnDataTable> columns = GetColumnDataTable();
			IList<PersonalInformationViewModel> personInf = SearchWith(x => !x.DT_Deleted.HasValue && x.FK_Person == OwnHelper.ConvertStringToInt(person.Id_Person)).ToList();
			personInf.ToList().ForEach(x => x.GetActions());
			return new PersonalInformationCache() {
				ListColumnsDataTable = columns,
					ListData = personInf
			};
		}

		private IList<ColumnDataTable> GetColumnDataTable()=>
		new List<ColumnDataTable>() {
			new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_PersonalInformation", Tx_ColumnTitle = "Id" },
				new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_TypeInformation", Tx_ColumnTitle = "Tipo" },
				new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_Person", Tx_ColumnTitle = "Id Persona" },
				new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "tx_FullNamePerson", Tx_ColumnTitle = "Nombre Persona" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_DescriptionCategoryInformation", Tx_ColumnTitle = "Categoria" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_DescriptionTypeInformation", Tx_ColumnTitle = "Tipo" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_InformationDescription", Tx_ColumnTitle = "Valor" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
		};

		public IList<PersonalInformationViewModel> GetAllPerson()=>
		repository.GetAll().Select(x => new PersonalInformationViewModel(x)).ToList();

		public IList<PersonalInformationViewModel> SearchWith(Func<PersonalInformation, bool> Predicate)=>
		repository.SearchWithTypeAndPerson(Predicate).Select(x => new PersonalInformationViewModel(x)).ToList();
	}
}