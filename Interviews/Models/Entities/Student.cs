using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Interviews.Models.Entities {
		public class Student {
				[Key]
				[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
				public int ID_Student { get; set; }

				[Required]
				public int FK_Person { get; set; }

				[Required]
				public bool BO_Enabled { get; set; }

				public DateTime? DT_Deleted { get; set; }

				[ForeignKey("FK_Person")]
				public virtual Person Person { get; set; } 
		}
}