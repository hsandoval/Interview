using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Interviews.Models.Entities;
using Interviews.Models.Wizards;
using MoreLinq;

namespace Interviews.Models.ViewModels
{
    public class StudentViewModel : PersonViewModel
    {
        public int Id_Student { get; set; }

        [Required]
        public int Fk_Person { get; set; }

        [Required]
        public bool Bo_EnabledProf { get; set; }

        public DateTime? Dt_DeletedProf { get; set; }

        public virtual PersonViewModel Person { get; set; }

        public StudentViewModel() { }

        public StudentViewModel(Student student)
        {
            if (student == null)
            {
                return;
            }
            else
            {
                this.Bo_IsEmpty = false;
                this.Id_Student = student.ID_Student;
                this.Fk_Person = student.FK_Person;
                this.Bo_EnabledProf = student.BO_Enabled;
                this.Dt_DeletedProf = student.DT_Deleted;
                if (student.Person != null)
                {
                    this.Id_Person = student.Person.ID_Person.ToString();
                    this.Tx_FirtsName = student.Person.TX_FirtsName;
                    this.Tx_MiddleName = student.Person.TX_MiddleName;
                    this.Tx_LastName = student.Person.TX_LastName;
                    this.Tx_SecondLastName = student.Person.TX_SecondLastName;
                    this.Fk_Gender = student.Person.FK_Gender;
                    this.Dt_Birthday = student.Person.DT_Birthday;
                    this.Bo_Enabled = student.Person.BO_Enabled;
                    PersonalInformation MainPersonalInformation = student.Person.PersonalInformation.MinBy(x => x.NU_PriorityIdentification);
                    this.Fk_TypeDocumentIdentification = MainPersonalInformation.FK_Type;
                    this.Tx_DescriptionDocumentIdentification = MainPersonalInformation.TX_InformationDescription;
                    this.Bo_IsEmpty = false;
                    this.PersonalInformation = student.Person.PersonalInformation.Count() == 0 ? null : student.Person.PersonalInformation.Select(x => new PersonalInformationViewModel(x)).ToList();
                }
            }
        }

        public Student ConvertViewModelStudentToClass() => new Student
        {
            ID_Student = this.Id_Student,
            FK_Person = this.Fk_Person,
            BO_Enabled = this.Bo_EnabledProf,
            DT_Deleted = this.Dt_DeletedProf,
            Person = this.Person.ConvertViewModelToClass()
        };

        public void GetActionsProfessor() => this.Action = new Action(this.Id_Student);
    }
}