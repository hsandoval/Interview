﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using System.Threading.Tasks;
using Interviews.Models.Wizards; 

namespace Interviews.Models.Caches {

public class PersonalInformationRequiredCache : Cache<PersonalInformationRequiredViewModel> {
		readonly IPersonalInformationRequiredRepository repository = new PersonalInformationRequiredRepository();

		public PersonalInformationRequiredCache GetDataTableInformation() {
			IList<ColumnDataTable> columns = GetColumnDataTable();
			IList<PersonalInformationRequiredViewModel> personalInformationRequired = SearchWith(x => !x.DT_Deleted.HasValue);
			personalInformationRequired.ToList().ForEach(x => x.GetActions());
			return new PersonalInformationRequiredCache() {
				ListColumnsDataTable = columns,
					ListData = personalInformationRequired
			};
		}

		private IList<ColumnDataTable> GetColumnDataTable()=>
		new List<ColumnDataTable>() {
			new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_PersonalInformationRequired", Tx_ColumnTitle = "Id" },
				new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_AssociatedCategory", Tx_ColumnTitle = "Categoria asociada" },
				new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "bo_IsDropDownList", Tx_ColumnTitle = "Es lista desplegable" },
				new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "dt_Deleted", Tx_ColumnTitle = "Edad" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_AssociatedCategory", Tx_ColumnTitle = "Categoría" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_Description", Tx_ColumnTitle = "Descripción" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "bo_Enabled", Tx_ColumnTitle = "Habilitado" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
		};

		public IList<PersonalInformationRequiredViewModel> SearchWith(Func<PersonalInformationRequired, bool> Predicate)=>
		repository.SearchWithCategory(Predicate).Select(x => new PersonalInformationRequiredViewModel(x)).ToList();

		public PersonalInformationRequiredViewModel TryEdit(PersonalInformationRequiredViewModel viewModel) {
			PersonalInformationRequiredViewModel currentRecord = GetById(viewModel);
			PersonalInformationRequiredViewModel returnedRecord;

			if (currentRecord.IsNullOrEmpty())returnedRecord = Insert(viewModel);
			else returnedRecord = Update(viewModel);

			return returnedRecord;
		}

		public bool TryDelete(int idPersonalInformationRequired, bool DeleteLogical = true) {
			PersonalInformationRequiredViewModel currentRecord = GetById(new PersonalInformationRequiredViewModel { Id_PersonalInformationRequired = idPersonalInformationRequired });
			if (!currentRecord.IsNullOrEmpty()) {
				if (DeleteLogical)return LogicDelete(currentRecord);
				else return PhysicalDelete(currentRecord);
			}

			return false;
		}

		public PersonalInformationRequiredViewModel GetById(PersonalInformationRequiredViewModel viewModel)=>
		new PersonalInformationRequiredViewModel(repository.GetById(viewModel.Id_PersonalInformationRequired));

		PersonalInformationRequiredViewModel Insert(PersonalInformationRequiredViewModel viewModel)=>
		new PersonalInformationRequiredViewModel(repository.Insert(viewModel.ConvertViewModelToClass()));

		PersonalInformationRequiredViewModel Update(PersonalInformationRequiredViewModel viewModel)=>
		new PersonalInformationRequiredViewModel(repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_PersonalInformationRequired));

		bool LogicDelete(PersonalInformationRequiredViewModel viewModel) {
			viewModel.DeleteLogical();
			repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_PersonalInformationRequired);
			return true;
		}

		bool PhysicalDelete(PersonalInformationRequiredViewModel viewModel)=>
		repository.Delete(viewModel.ConvertViewModelToClass());
	}
}