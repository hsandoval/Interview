using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Interviews.Models.Entities;

namespace Interviews.Models.ViewModels
{
    public class SurveyServiceViewModel : ViewModel<SurveyServiceViewModel>
    {
        public int Id_SurveyServices { get; set; }

        [Required]
        public int Fk_Interview { get; set; }

        [Required]
        [Display(Name = "Persona")]
        public int Fk_Person { get; set; }

        public DateTime Dt_Created { get; set; }

        public virtual InterviewViewModel Interviews { get; set; }

        public virtual PersonViewModel Person { get; set; }

        public SurveyServiceViewModel() { }

        public SurveyServiceViewModel(SurveyService surveyServices)
        {
            if (surveyServices == null)
            {
                return;
            }
            this.Bo_IsEmpty = false;
            this.Id_SurveyServices = surveyServices.ID_SurveyService;
            this.Fk_Interview = surveyServices.FK_Interview;
            this.Fk_Person = surveyServices.FK_Person;
            this.Dt_Created = surveyServices.DT_Created;
        }

        public SurveyService ConvertViewModelToClass() => new SurveyService
        {
            ID_SurveyService = this.Id_SurveyServices,
            FK_Interview = this.Fk_Interview,
            FK_Person = this.Fk_Person,
            DT_Created = this.Dt_Created
        };

        public void GetActions() => this.Action = new Action(this.Id_SurveyServices);
    }
}