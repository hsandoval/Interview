﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;

namespace Interviews.Models.Caches {
		public class StudentCache : Cache<StudentViewModel> {
				readonly IStudentRepository repository = new StudentRepository();

				public StudentCache GetDataTableInformation() {
						IList<ColumnDataTable> columns = GetColumnDataTable();
						IList<StudentViewModel> students = SearchWith(x => !x.DT_Deleted.HasValue);
						students.ToList().ForEach(x => x.GetActions());
						return new StudentCache() {
								ListColumnsDataTable = columns,
								ListData = students
						};
				}

				private IList<ColumnDataTable> GetColumnDataTable() =>
				new List<ColumnDataTable>() {
			new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_Person", Tx_ColumnTitle = "Id" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_FullName", Tx_ColumnTitle = "Nombre" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "dt_Birthday", Tx_ColumnTitle = "Fecha Nacimiento" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "nu_Age", Tx_ColumnTitle = "Edad" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
				};

				public IList<StudentViewModel> SearchWith(Func<Student, bool> Predicate) =>
					repository.SearchWithStudent(Predicate).Select(x => new StudentViewModel(x)).ToList();

				public StudentViewModel TryEdit(StudentViewModel viewModel) {
						StudentViewModel currentRecord = GetById(viewModel);
						StudentViewModel returnedRecord;

						if (currentRecord.IsNullOrEmpty())
								returnedRecord = Insert(viewModel);
						else
								returnedRecord = Update(viewModel);

						return returnedRecord;
				}

				public StudentViewModel GetById(StudentViewModel viewModel) =>
				new StudentViewModel(repository.SearchWithStudent(x => x.ID_Student == viewModel.Id_Student).SingleOrDefault());

				StudentViewModel Insert(StudentViewModel viewModel) {

						viewModel.Person.PersonalInformation = new List<PersonalInformationViewModel> {
								new PersonalInformationViewModel {
										Nu_PriorityIdentification = 1,
										Fk_TypeInformation = viewModel.Person.Fk_TypeDocumentIdentification,
										Tx_InformationDescription = viewModel.Person.Tx_DescriptionDocumentIdentification
								}
						};

						return new StudentViewModel(repository.Insert(viewModel.ConvertViewModelStudentToClass()));
				}

				StudentViewModel Update(StudentViewModel viewModel) =>
				new StudentViewModel(repository.Update(viewModel.ConvertViewModelStudentToClass(), viewModel.Id_Student));

				public bool TryDelete(int id_Student, bool DeleteLogical = true) {
						StudentViewModel currentRecord = GetById(new StudentViewModel { Id_Student = id_Student });
						if (!currentRecord.IsNullOrEmpty()) {
								if (DeleteLogical)
										return LogicDelete(currentRecord);
								else
										return PhysicalDelete(currentRecord);
						}

						return false;
				}

				bool LogicDelete(StudentViewModel viewModel) {
						viewModel.DeleteLogical();
						repository.Update(viewModel.ConvertViewModelStudentToClass(), OwnHelper.ConvertStringToInt(viewModel.Id_Person));
						return true;
				}

				bool PhysicalDelete(StudentViewModel viewModel) =>
				repository.Delete(viewModel.ConvertViewModelStudentToClass());

				public IList<StudentViewModel> GetAllStudents() =>
						repository.GetAllStudents().Select(x => new StudentViewModel(x)).ToList();
		}
}