﻿using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Repositories
{
    public interface IUserRepository : IRepository<Person>
    {
        List<Person> GetAllUsers(Func<Person, bool> Predicate);
    }
    public class UserRepository : Repository<Person>, IUserRepository
    {
        public List<Person> GetAllUsers(Func<Person, bool> Predicate) =>
            _EF.Persons
            .Include(x => x.UserApplication)
            .ToList();
    }
}
