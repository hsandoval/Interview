﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Interviews.Models.Entities;

namespace Interviews.Models.ViewModels
{
    public class TypeViewModel : ViewModel<TypeViewModel>
    {
        [Display(Name = "Id tipo")]
        public int Id_Type { get; set; }

        [Display(Name = "Descripción")]
        [StringLength(50, ErrorMessage = "El nombre debe ser de mínimo dos (2) y máximo cincuenta (50) caracteres.", MinimumLength = 2)]
        [Required(ErrorMessage = "El campo nombre es requerido")]
        public string Tx_Description { get; set; }

        [Display(Name = "Nombre icono")]
        public string Tx_NameIconFontAwesome { get; set; }

        [Display(Name = "Eliminado")]
        public bool Bo_Deleted { get; set; }

        [Display(Name = "Padre")]
        public int? Fk_ParentType { get; set; }

        [Display(Name = "Categoria")]
        public int Fk_Category { get; set; }

        bool Bo_IncludeRegistersDeleted { get; set; }

        public virtual IList<TypeViewModel> ChildrenTypes { get; set; }

        public IList<TreeViewViewModel> TreeView { get; set; }

        public TypeViewModel() { }

        public TypeViewModel(Entities.Type type)
        {
            if (type == null)
            {
                return;
            }
            else
            {
                Bo_IsEmpty = false;
                Id_Type = type.ID_Type;
                Tx_Description = type.TX_Description;
                //Tx_NameIconFontAwesome = type.TX_NameIconFontAwesome;
                Bo_Enabled = type.BO_Enabled;
                Bo_Deleted = type.DT_Deleted.HasValue;
                Dt_Deleted = type.DT_Deleted;
                Fk_ParentType = type.FK_ParentType;
                Fk_Category = type.FK_Category;

                if (type.ChildrenTypes.Any())
                {
                    ChildrenTypes = new List<TypeViewModel>();
                    type.ChildrenTypes.ToList().ForEach(x => ChildrenTypes.Add(new TypeViewModel(x)));
                }
            }
        }

        public Entities.Type ConvertViewModelToClass() => new Entities.Type
        {
            ID_Type = this.Id_Type,
            TX_Description = this.Tx_Description,
            //TX_NameIconFontAwesome = this.Tx_NameIconFontAwesome,
            BO_Enabled = this.Bo_Enabled,
            DT_Deleted = this.Dt_Deleted,
            FK_ParentType = this.Fk_ParentType,
            FK_Category = this.Fk_Category
        };
    }

    public class TreeViewViewModel
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Parent { get; set; }
        public string Icon { get; set; }

        public TreeViewViewModel(CategoryViewModel category)
        {
            Id = category.Id_Category.ToString();
            Text = category.Tx_Description;
            Parent = "#";
            Icon = category.Type.Tx_NameIconFontAwesome;
        }

        public TreeViewViewModel(TypeViewModel type)
        {
            Id = type.Id_Type.ToString();
            Text = type.Tx_Description;
            Parent = type.Fk_ParentType.HasValue ? type.Fk_ParentType.ToString() : "#";
            Icon = type.Tx_NameIconFontAwesome;
        }
    }
}