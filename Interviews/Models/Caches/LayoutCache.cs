﻿namespace Interviews.Models.Caches
{
    public class LayoutCache
    {
        public string Title { get; set; }

        public LayoutCache(string Title) => this.Title = Title;
    }

}