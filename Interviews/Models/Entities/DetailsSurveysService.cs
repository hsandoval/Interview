﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Entities
{
    public class DetailSurveyService
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int ID_DetailsSurveysServices { get; set; }

        [Required]
        public int FK_SurveyServices { get; set; }

        [Required]
        public int FK_Ask { get; set; }

        [Required]
        public int NU_GradeSatisfaction { get; set; }

        [Required]
        public string TX_Response { get; set; }

        [ForeignKey("FK_SurveyServices")]
        public virtual SurveyService SurveyServices { get; set; }

        [ForeignKey("FK_Ask")]
        public virtual Type Ask { get; set; }

        [ForeignKey("NU_GradeSatisfaction")]
        public virtual Type GradeSastisfaction { get; set; }
    }
}
