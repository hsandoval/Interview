using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Interviews.Models.Entities;

namespace Interviews.Models.ViewModels
{
    public class SheduleByProfessorViewModel : ViewModel<SheduleByProfessorViewModel>
    {
        public int Id_SheduleByProfessor { get; set; }

        [Required]
        public int Fk_Professor { get; set; }

        [Required]
        [Display(Name = "Desde")]
        public DateTime Dt_From { get; set; }

        [Required]
        [Display(Name = "Hasta")]
        public DateTime Dt_To { get; set; }

        public virtual ProfessorViewModel Professor { get; set; }

        public virtual FullCalendarViewModel FullCalendarViewModel { get; set; }

        public SheduleByProfessorViewModel() { }

        public SheduleByProfessorViewModel(SheduleByProfessor shedule)
        {
            if (shedule == null)
            {
                return;
            }
            this.Bo_IsEmpty = false;
            this.Id_SheduleByProfessor = shedule.ID_SheduleByProfessor;
            this.Fk_Professor = shedule.FK_Professor;
            this.Dt_From = shedule.DT_From;
            this.Dt_To = shedule.DT_To;
            this.Bo_Enabled = shedule.BO_Enabled;
            this.Dt_Deleted = shedule.DT_Deleted;
        }

        public SheduleByProfessor ConvertViewModelToClass() => new SheduleByProfessor
        {
            ID_SheduleByProfessor = this.Id_SheduleByProfessor,
            FK_Professor = this.Fk_Professor,
            DT_From = this.Dt_From,
            DT_To = this.Dt_To,
            BO_Enabled = this.Bo_Enabled,
            DT_Deleted = this.Dt_Deleted,
        };

        public void GetActions() => this.Action = new Action(this.Id_SheduleByProfessor);
    }
}