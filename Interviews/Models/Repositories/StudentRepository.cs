﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Interviews.Models.Repositories {
		public interface IStudentRepository : IRepository<Student> {
				List<Student> SearchWithStudent(Func<Student, bool> predicate);
				List<Student> GetAllStudents();
		}
		public class StudentRepository : Repository<Student>, IStudentRepository {
				public List<Student> SearchWithStudent(Func<Student, bool> predicate) =>
					_EF.Students
					.Include(x => x.Person)
					.Include(x => x.Person.PersonalInformation)
					.Where(predicate)
					.ToList();

				public List<Student> GetAllStudents() =>
						_EF.Students
						.Include(x => x.Person)
						.ThenInclude(x => x.PersonalInformation)
						.ToList();
		}
}