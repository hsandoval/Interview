﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Interviews.Models.Repositories
{
    public interface IProfessorRepository : IRepository<Professor>
    {
        List<Professor> SearchWithProfessor(Func<Professor, bool> predicate);
        List<Professor> GetAllProfessors();
    }
    public class ProfessorRepository : Repository<Professor>, IProfessorRepository
    {
        public List<Professor> SearchWithProfessor(Func<Professor, bool> predicate) =>
                _EF.Professors
                .Include(x => x.Person)
                .ThenInclude(x => x.PersonalInformation)
                .Where(predicate)
                .ToList();

        public List<Professor> GetAllProfessors() =>
                _EF.Professors
                .Include(x => x.Person)
                .Include(x => x.Person.PersonalInformation)
                .ToList();
    }
}