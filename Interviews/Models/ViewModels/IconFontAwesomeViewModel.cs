﻿using Interviews.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.ViewModels
{
    public class IconFontAwesomeViewModel
    {
        [Required]
        [Display(Name = "Id")]
        public int Id_IconFontAwesome { get; set; }

        [Required]
        [Display(Name = "Icono")]
        public string Tx_Description { get; set; }

        public IconFontAwesomeViewModel()
        {
        }

        public IconFontAwesomeViewModel(IconFontAwesome iconFontAwesome)
        {
            this.Id_IconFontAwesome = iconFontAwesome.ID_IconFontAwesome;
            this.Tx_Description = iconFontAwesome.TX_Description;
        }
    }
}
