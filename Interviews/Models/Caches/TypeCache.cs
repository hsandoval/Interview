﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;

namespace Interviews.Models.Caches {

	public class TypeCache : Cache<TypeViewModel> {
		#region BodyClass

		readonly ITypeRepository repository = new TypeRepository();

		public IList<TreeViewViewModel> TreeHierarchy { get; set; }

		#endregion

		#region Methods

		public TypeViewModel GetById(int idCategory)=>
		new TypeViewModel(repository.GetById(idCategory));

		public TypeViewModel TryEdit(TypeViewModel viewModel) {
			TypeViewModel currentRecord = GetById(viewModel.Id_Type);

			if (currentRecord.IsNullOrEmpty()) {
				return Insert(viewModel);
			} else {
				Update(viewModel);
				return viewModel;
			}
		}

		TypeViewModel Insert(TypeViewModel viewModel)=>
		new TypeViewModel(repository.Insert(viewModel.ConvertViewModelToClass()));

		public void Delete(int idCategory, bool isDeleteLogical = true) {
			Entities.Type currentRecord = repository.GetById(idCategory);

			if (currentRecord != null) {

				var viewModel = new TypeViewModel(currentRecord);

				if (isDeleteLogical) {
					DeleteLogical(viewModel);
					return;
				}
				DeletePhysical(viewModel);
			}
		}

		TypeViewModel Update(TypeViewModel viewModel)=>
		new TypeViewModel(repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_Type));

		public TypeViewModel GetTypeWithChildrenTypes(TypeViewModel viewModel) =>
		repository.GetTypesByPredicate(x => x.ID_Type == viewModel.Id_Type).Select(x => new TypeViewModel(x)).SingleOrDefault();

		public IList<TypeViewModel> SearchWith(Func<Entities.Type, bool> predicate)=>
		repository.SearchWith(predicate).Select(x => new TypeViewModel(x)).ToList();

		#endregion

		#region Helpers

		void DeleteLogical(TypeViewModel viewModel) {
			viewModel.Dt_Deleted = DateTime.Now;
			viewModel.Bo_Enabled = false;
			repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_Type);
		}

		void DeletePhysical(TypeViewModel viewModel)=>
		repository.Delete(viewModel.ConvertViewModelToClass());

		void OrderTree(TypeViewModel type) {
			if (!type.ChildrenTypes.IsNullOrEmpty()) {
				type.ChildrenTypes.ToList().ForEach(x => OrderTree(x));
			}
			this.TreeHierarchy.Add(new TreeViewViewModel(type));
		}

		void OrderTree(CategoryViewModel category) {
			this.TreeHierarchy.Add(new TreeViewViewModel(category));
		}
		#endregion
	}
}