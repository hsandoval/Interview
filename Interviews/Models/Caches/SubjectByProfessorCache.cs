using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;

namespace Interviews.Models.Caches
{
    public class SubjectByProfessorCache : Cache<SubjectByProfessorViewModel>
    {
        readonly ISubjectByProfessorRepository repository = new SubjectByProfessorRepository();

        public SubjectByProfessorCache GetDataTable()
        {
            IList<ColumnDataTable> columns = GetColumnDataTable();
            IList<SubjectByProfessorViewModel> subjectsByProfessors = SearchWith(x => !x.DT_Deleted.HasValue).ToList();
            subjectsByProfessors.ToList().ForEach(x => x.GetActions());
            return new SubjectByProfessorCache()
            {
                ListColumnsDataTable = columns,
                ListData = subjectsByProfessors
            };
        }

        private IList<ColumnDataTable> GetColumnDataTable() =>
            new List<ColumnDataTable>() {
                new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_SubjectByProfessor", Tx_ColumnTitle = "Id" },
                new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_Professor", Tx_ColumnTitle = "Profesor" },
                new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_Subject", Tx_ColumnTitle = "Area de conocimiento" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_FullNameProfessor", Tx_ColumnTitle = "Profesor" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_Subject", Tx_ColumnTitle = "Area de conocimiento" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "bo_Enabled", Tx_ColumnTitle = "Habilitado" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
            };

        internal void Delete(int iD_Record)
        {
            throw new NotImplementedException();
        }

        public IList<SubjectByProfessorViewModel> GetAll() =>
            repository.GetAll().Select(x => new SubjectByProfessorViewModel(x)).ToList();

        public IList<SubjectByProfessorViewModel> SearchWith(Func<SubjectByProfessor, bool> Predicate = null) =>
            repository.SearchWithSubjectsAndProfessors(Predicate).Select(x => new SubjectByProfessorViewModel(x)).ToList();

        public SubjectByProfessorViewModel TryEdit(SubjectByProfessorViewModel viewModel)
        {
            SubjectByProfessorViewModel currentRecord = GetById(viewModel);
            SubjectByProfessorViewModel returnedRecord;

            if (currentRecord.IsNullOrEmpty())
                returnedRecord = Insert(viewModel);
            else
                returnedRecord = Update(viewModel);

            return returnedRecord;
        }

        public SubjectByProfessorViewModel GetById(SubjectByProfessorViewModel viewModel)
        {
            var data = SearchWith(x => x.ID_SubjectByProfessor == viewModel.ConvertViewModelToClass().ID_SubjectByProfessor).FirstOrDefault();
            return data ?? new SubjectByProfessorViewModel();
        }

        SubjectByProfessorViewModel Insert(SubjectByProfessorViewModel viewModel) =>
            new SubjectByProfessorViewModel(repository.Insert(viewModel.ConvertViewModelToClass()));

        SubjectByProfessorViewModel Update(SubjectByProfessorViewModel viewModel) =>
            new SubjectByProfessorViewModel(repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_SubjectByProfessor));

        public IList<ProfessorViewModel> GetProfessorBySubject(SubjectByProfessorViewModel viewModel) =>
            repository.SearchWithSubjectsAndProfessors(x =>
                x.FK_Subject == viewModel.Fk_Subject &&
                x.BO_Enabled &&
                !x.DT_Deleted.HasValue
            ).Select(x => new ProfessorViewModel(x.Professor)).ToList();

    }
}