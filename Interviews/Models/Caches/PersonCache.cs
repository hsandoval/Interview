﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;

namespace Interviews.Models.Caches {
	public class PersonCache : Cache<PersonViewModel> {
		readonly IPersonRepository repository = new PersonRepository();

		public PersonCache GetDataTableInformation() {
			IList<ColumnDataTable> columns = GetColumnDataTable();
			IList<PersonViewModel> persons = SearchWith(x => !x.DT_Deleted.HasValue);
			persons.ToList().ForEach(x => x.GetActions());
			return new PersonCache() {
				ListColumnsDataTable = columns,
					ListData = persons
			};
		}

		public PersonCache GetDataTableInformationByIdPerson(int idPerson) {
			IList<ColumnDataTable> columns = GetColumnDataTable();
			PersonViewModel person = SearchWith(x => !x.DT_Deleted.HasValue && x.ID_Person == idPerson).SingleOrDefault();
			if (person != null)person.GetActions();
			return new PersonCache() {
				ListColumnsDataTable = columns,
					DataOnly = person
			};
		}

		private IList<ColumnDataTable> GetColumnDataTable()=>
		new List<ColumnDataTable>() {
			new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_Person", Tx_ColumnTitle = "Id" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_FullName", Tx_ColumnTitle = "Nombre" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "dt_Birthday", Tx_ColumnTitle = "Fecha Nacimiento" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "nu_Age", Tx_ColumnTitle = "Edad" },
				new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
		};

		public IList<PersonViewModel> GetAllPerson()=>
		repository.GetAll().Select(x => new PersonViewModel(x)).ToList();

		public IList<PersonViewModel> SearchWith(Func<Person, bool> Predicate)=>
		repository.SearchWith(Predicate).Select(x => new PersonViewModel(x)).ToList();

		public PersonViewModel TryEdit(PersonViewModel viewModel) {
			PersonViewModel currentRecord = GetPersonById(viewModel);
			PersonViewModel returnedRecord;

			if (currentRecord.IsNullOrEmpty())returnedRecord = Insert(viewModel);
			else returnedRecord = Update(viewModel);

			return returnedRecord;
		}

		public bool TryDelete(int idPerson, bool DeleteLogical = true) {
			PersonViewModel currentRecord = GetPersonById(new PersonViewModel { Id_Person = idPerson.ToString() });
			if (!currentRecord.IsNullOrEmpty()) {
				if (DeleteLogical)return LogicDelete(currentRecord);
				else return PhysicalDelete(currentRecord);
			}

			return false;
		}

		public PersonViewModel GetPersonById(PersonViewModel viewModel)=>
		new PersonViewModel(repository.GetById(OwnHelper.ConvertStringToInt(viewModel.Id_Person)));

		public PersonViewModel GetPersonByDocumentIdentity(PersonViewModel viewModel) {
			throw new NotImplementedException();
		}

		PersonViewModel Insert(PersonViewModel viewModel)=>
		new PersonViewModel(repository.Insert(viewModel.ConvertViewModelToClass()));

		PersonViewModel Update(PersonViewModel viewModel)=>
		new PersonViewModel(repository.Update(viewModel.ConvertViewModelToClass(), OwnHelper.ConvertStringToInt(viewModel.Id_Person)));

		bool LogicDelete(PersonViewModel viewModel) {
			viewModel.DeleteLogical();
			repository.Update(viewModel.ConvertViewModelToClass(), OwnHelper.ConvertStringToInt(viewModel.Id_Person));
			return true;
		}

		bool PhysicalDelete(PersonViewModel viewModel)=>
		repository.Delete(viewModel.ConvertViewModelToClass());
	}
}