﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interviews.Models.Entities;
using Interviews.Models.Repositories;
using Interviews.Models.ViewModels;
using Microsoft.IdentityModel.Tokens;

namespace Interviews.Models.Caches
{
    public class InterviewCache : Cache<InterviewViewModel>
    {
        readonly IInterviewRepository repository = new InterviewRepository();

        public InterviewCache GetDataTableInformation()
        {
            IList<ColumnDataTable> columns = GetColumnDataTable();
            IList<InterviewViewModel> interviews = SearchWith(x => !x.DT_Deleted.HasValue);
            interviews.ToList().ForEach(x => x.GetActions());
            return new InterviewCache()
            {
                ListColumnsDataTable = columns,
                ListData = interviews
            };
        }


        public InterviewCache GetInterviewsByStudent(InterviewViewModel viewModel)
        {
            IList<ColumnDataTable> columns = GetColumnDataTable();
            IList<InterviewViewModel> interviews = SearchWith(x => !x.DT_Deleted.HasValue && x.FK_Student == viewModel.Fk_Student);
            interviews.ToList().ForEach(x => { x.GetActions(); x.SetStateRedirectInterview(); });
            return new InterviewCache()
            {
                ListColumnsDataTable = columns,
                ListData = interviews
            };
        }

        private IList<ColumnDataTable> GetColumnDataTable() =>
        new List<ColumnDataTable>() {
            new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "id_Interview", Tx_ColumnTitle = "Id" },
                new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_Professor", Tx_ColumnTitle = "Id profesor" },
                new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_Student", Tx_ColumnTitle = "Id estudiante" },
                new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_StateInterview", Tx_ColumnTitle = "Id estatus" },
                new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "fk_Topic", Tx_ColumnTitle = "Id Tópico" },
                new ColumnDataTable() { Bo_Hidden = true, Tx_ColumnName = "action", Tx_ColumnTitle = "Acciones" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "dt_Interview", Tx_ColumnTitle = "Fecha" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_FullNameProfessor", Tx_ColumnTitle = "Nombre profesor" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_FullNameStudent", Tx_ColumnTitle = "Nombre estudiante" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_StateInterview", Tx_ColumnTitle = "Estatus" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_Topic", Tx_ColumnTitle = "Tópico" },
                new ColumnDataTable() { Bo_Hidden = false, Tx_ColumnName = "tx_TagHtmlRedirect", Tx_ColumnTitle = "Salas" },
        };

        public InterviewCache GetDataTableInformationByIdProfessor(InterviewViewModel interview)
        {
            IList<ColumnDataTable> columns = GetColumnDataTable();
            InterviewViewModel interviews = SearchWith(x => !x.DT_Deleted.HasValue && x.FK_Professor == interview.Fk_Professor).SingleOrDefault();
            if (interviews != null)
                interviews.GetActions();
            return new InterviewCache()
            {
                ListColumnsDataTable = columns,
                DataOnly = interviews
            };
        }

        public InterviewCache GetDataTableInformationByIdStudent(StudentViewModel student)
        {
            IList<ColumnDataTable> columns = GetColumnDataTable();
            InterviewViewModel interviews = SearchWith(x => !x.DT_Deleted.HasValue && x.FK_Student == student.Id_Student).SingleOrDefault();
            if (interviews != null)
                interviews.GetActions();
            return new InterviewCache()
            {
                ListColumnsDataTable = columns,
                DataOnly = interviews
            };
        }

        public IList<InterviewViewModel> GetAll() =>
        repository.GetAll().Select(x => new InterviewViewModel(x)).ToList();

        public IList<InterviewViewModel> SearchWith(Func<Interview, bool> Predicate) =>
        repository.GetAllInterviews(Predicate).Select(x => new InterviewViewModel(x)).ToList();

        public InterviewViewModel TryEdit(InterviewViewModel viewModel)
        {
            GetJWT();
            InterviewViewModel currentRecord = new InterviewViewModel(repository.GetById(viewModel.Id_Interview));
            InterviewViewModel returnedRecord;

            if (currentRecord.IsNullOrEmpty())
            {
                viewModel.Fk_StateInterview = 8;
                viewModel.Tx_UrlInterview = GetUrlConnection();
                returnedRecord = Insert(viewModel);
            }
            else
                returnedRecord = Update(viewModel);

            return returnedRecord;
        }

        InterviewViewModel Insert(InterviewViewModel viewModel) => new InterviewViewModel(repository.Insert(viewModel.ConvertViewModelToClass()));

        InterviewViewModel Update(InterviewViewModel viewModel) => new InterviewViewModel(repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_Interview));

        public void Delete(int idInterview, bool isDeleteLogical = true)
        {
            Interview currentRecord = repository.GetById(idInterview);

            if (currentRecord != null)
            {

                var viewModel = new InterviewViewModel(currentRecord);

                if (isDeleteLogical)
                {
                    DeleteLogical(viewModel);
                    return;
                }
                DeletePhysical(viewModel);
            }
        }

        void DeleteLogical(InterviewViewModel viewModel)
        {
            viewModel.Dt_Deleted = DateTime.Now;
            viewModel.Bo_Enabled = false;
            repository.Update(viewModel.ConvertViewModelToClass(), viewModel.Id_Interview);
        }

        void DeletePhysical(InterviewViewModel viewModel) =>
        repository.Delete(viewModel.ConvertViewModelToClass());

        string GetUrlConnection() => 
            "https://interviews.skype.com/es/interviews?code=5d118c8e-e493-eecf-4609-1763b8fa16eb";

        void GetJWT()
        {
            DateTime epoch = new DateTime(1970, 1, 1);
            int seconds = (int)Math.Floor((DateTime.Now - epoch).TotalSeconds);

            string key = "570c30d0-adcd-4567-fe74-b05a3003b72d";

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var header = new JwtHeader(credentials);

            var payload = new JwtPayload
            {
                { "jti", "d8661a14-4b7c-5fda-2227-9b055fcf5b10" },
                { "iss", "b140b7bd-cb62-873b-2979-937c50f15751" },
                { "iat", seconds },
                { "sub", "44136fa355b3678a1146ad16f7e8649e94fb4fc21fe77e8310c060f61caaff8a" },
                { "exp", seconds + 10 }
            };

            var secToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();

            var tokenString = handler.WriteToken(secToken);
        }
    }
}