let CreateDataTable = (idTagHtml) => {
    const tagHtml = $(`#${idTagHtml}`);

    let customTags = tagHtml.data('options');
    const urlRequest = `${customTags.Controller}/${customTags.MethodSelectAll.Method}`;

    let obj;
    if ('Parameter' in customTags.MethodSelectAll) {
        obj = customTags.MethodSelectAll.Parameter;
    } else {
        obj = {};
    }

    let HttpRequest = MakeRequest(urlRequest, obj);

    let modalId = customTags.ModalId;

    HttpRequest
        .then(res => {
            const dataSet = $.parseJSON(res.body);
            const columns = GetColumns(dataSet.listColumnsDataTable);
            const dataBody = GetDataBody(dataSet.listData);
            tagHtml.DataTable({
                language: {
                    'url': '//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json'
                },
                data: dataBody,
                columns: columns[0],
                columnDefs: columns[1],
                responsive: true,
                dom: '<lf<t>ip>',
                pageLength: 50,
                buttons: [{
                    extend: 'excel',
                    title: 'titleDataTable',
                    exportOptions: {
                        orthogonal: 'sort',
                        columns: 'columnsToPrinter'
                    }
                }, {
                    extend: 'pdf',
                    title: 'titleDataTable',
                    orientation: 'orientationSheetPDF',
                    exportOptions: {
                        orthogonal: 'sort',
                        columns: 'columnsToPrinter'
                    }
                }]
            });

            if (customTags.MethodTryEdit.Type === 'Redirect') {
                Redirect(idTagHtml, modalId);
            } else {
                FillModal(idTagHtml);
            }
            DeleteRecord(idTagHtml);

        }).catch((err) => {
            console.log(`Algo salió mal: ${err}`);
        });
}

let GetColumns = (listColumns) => {
    let columns = [];
    let columnsDefs = [];
    $.each(listColumns, function (index, item) {
        let obj = {};
        obj.data = item['tx_ColumnName'];
        obj.title = item['tx_ColumnTitle'];
        columns.push(obj);
        if (item['bo_Hidden'] == true) {
            let obj = {};
            obj.targets = index;
            obj.visible = false;
            obj.searchable = false;
            columnsDefs.push(obj);
        }
    });
    return [columns, columnsDefs];
}

let GetDataBody = (dataBody) => {
    $.each(dataBody, (index, item) => {
        item = fnGetDataRow(item);
    });
    return dataBody;
}

let fnGetDataRow = (item) => {
    let tagHtml = `
	<div class="input-group-prepend">
		<button class="btn btn-primary" type="button">Acciones</button>
		<button class="btn dropdown-toggle btn-primary" type="button" data-toggle="dropdown">
		</button>
		<ul class="dropdown-menu menu-data-table" role="menu">
			${item.action.tx_Edit}
			${item.action.tx_Delete}
		</ul>
	</div>
	`;
    item.action = tagHtml;
    return item;
}

let Redirect = (idTable, modalId) => {
    var table = $(`#${idTable}`).DataTable();

    $(`#${idTable} tbody`).on('click', '#editRecord', function () {
        const tagHtml = $(`#${idTable}`);

        let customTags = tagHtml.data('options');
        let urlRequest = `${customTags.Controller}/${customTags.MethodTryEdit.Method}`;

        let row = table.row($(this).parents('tr')[0]).data();

        const params = {
            id: row[customTags.MethodTryEdit.Parameter]
        };

        urlRequest = new URL(`${window.location.protocol}//${window.location.host}/${urlRequest}`);
        Object.keys(params).forEach(key => urlRequest.searchParams.append(key, params[key]));

        window.location.href = urlRequest.href;
    });
}

let FindRecord = (table, row) => {
}

let DeleteRecord = (idTagHtml) => {
    var table = $(`#${idTagHtml}`).DataTable();
    $(`#${idTagHtml} tbody`).on('click', '#deleteRecord', function () {
        let row = table.row($(this).parents('tr')[0]).data();
        ConfirmDeleteRecord(row, idTagHtml);
    });
}

let ConfirmDeleteRecord = (Record, idTagHtml) => {
    const tagHtml = $(`#${idTagHtml}`);

    let customTags = tagHtml.data('options');
    const urlRequest = `${customTags.Controller}/${customTags.MethodTryDelete}`;

    swal({
        title: '¿Estás seguro?',
        text: '¡Vas a eliminar el registro y todas sus dependencias!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, eliminar!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            let HttpRequest = DeleteData(urlRequest, Record);

            HttpRequest
                .then(res => {
                    swal({
                        title: 'Registro eliminado!',
                        text: 'El registro fue eliminado exitosamente.',
                        type: 'success'
                    });
                })
                .catch(err => {
                    swal({
                        title: 'Poof! Ha ocurrido un error!',
                        text: err,
                        icon: 'error',
                    });
                });
        } else {
            swal({
                title: 'Acción cancelada',
                text: 'El registro se encuentra a salvo :)',
                type: 'info'
            });
        }
    });
}

async function InsertNewRow(idTable, urlRequest, obj) {
    let response = await ConfirmInsertActionReturnData(urlRequest, obj);

    const tagHtml = $(`#${idTable}`);
    AddNewRow(idTable, response.body);
}

let OpenModalDataTable = (idForm, tittleModal, clear = false) => {
    if (clear) FormClear(idForm);
    $('#ModalTitle').html(tittleModal);
    $(`#${idForm}`).modal();
    $('.modal').insertBefore($('#contentApplication'));
}

let FillModal = (idTable) => {
    var table = $(`#${idTable}`).DataTable();

    $(`#${idTable} tbody`).on('click', '#editRecord', function () {
        const tagHtml = $(`#${idTable}`);

        let customTags = tagHtml.data('options');
        const urlRequest = `${customTags.Controller}/${customTags.MethodTryEdit.Method}`;

        let row = table.row($(this).parents('tr')[0]).data();

        const params = {
            id: row[customTags.MethodTryEdit.Parameter]
        };

        let HttpRequest = MakeRequestGET(urlRequest, params);

        HttpRequest
            .then(res => {
                OpenModalDataTable(customTags.MethodTryEdit.IdModal, 'Agregar nueva información', true);
                $('.modal-body').empty();
                $('.modal-body').append(res.body);
            }).catch((err) => {
                console.log(`Algo salió mal: ${err}`);
            });
    });
}

function AddNewRow(idTable, obj) {
    var table = $(`#${idTable}`).DataTable();

    const dataSet = $.parseJSON(obj);
    const columns = GetColumns(dataSet.listColumnsDataTable);
    const dataBody = fnGetDataRow(dataSet.dataOnly);

    table.row.add(dataBody).draw(false);
}
// Función para formatear el datatable de las listas con una clase determinada
// Parametros: 
//  *claseDataTable: Índica el nombre de la clase que contiene la tabla
//  *titulo: Es el título que saldrá en el documento si es exportado a excel, pdf o impresión
//  *columnasAImprimir: Es un arreglo que indica las posiciones de las columnas que se imprimirán,
//  *columnasConCheckBox: Es un arreglo que indica cuáles columnas son checkbox para que en el columnDefs sean formateados
function formatDataTable(classDataTable, titleDataTable, columnsToPrinter, columnsWithCheckBox, orientationSheetPDF) {
    if (typeof orientationSheetPDF === 'undefined')
        orientationSheetPDF = 'vertical';
    $(classDataTable).DataTable({
        language: {
            'url': 'http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
        },
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        pageLength: 50,
        buttons: [{
            extend: 'excel',
            title: titleDataTable,
            exportOptions: {
                orthogonal: 'sort',
                columns: columnsToPrinter
            }
        }, {
            extend: 'pdf',
            title: titleDataTable,
            orientation: orientationSheetPDF,
            exportOptions: {
                orthogonal: 'sort',
                columns: columnsToPrinter
            }
        }, {
            extend: 'print',
            text: 'Imprimir',
            exportOptions: {
                orthogonal: 'sort',
                columns: columnsToPrinter
            },
            customize: function (win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
            }
        }
        ], columnDefs: [{
            targets: columnsWithCheckBox,
            render: function (data, type, row, meta) {
                if (type === 'sort') {
                    var $input = $(data).find('input[type="checkbox"]').addBack();
                    data = ($input.prop('checked')) ? 'Activo' : 'Inactivo';
                }
                return data;
            }
        }]
    });
}