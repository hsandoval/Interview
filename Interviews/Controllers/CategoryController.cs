using System;
using System.Collections.Generic;
using System.Linq;
using Interviews.Models.Caches;
using Interviews.Models.Entities;
using Interviews.Models.Enums;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class CategoryController : Controller
    {

        public ILogger ILogger { get; set; }
        public CategoryController(ILogger<CategoryController> Logger)
        {
            ILogger = Logger;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Categorias";
            ViewBag.TextButton = "Agregar nueva categoria";
            ViewBag.FontAwesomeIcon = "fas fa- th-list";
            ViewBag.Controller = "Category";
            ViewBag.Parameter = "id_Category";
            return View();
        }

        [HttpPost]
        public ActionResult TryPopulateDataTable()
        {
            try
            {
                CategoryCache cache = new CategoryCache();
                CategoryCache dataTable = cache.GetDataTableInformation();
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        public IActionResult Create() =>
            View("CategoryForm");

        public IActionResult TryGetCategory(int id)
        {
            CategoryCache cache = new CategoryCache();

            try
            {
                CategoryViewModel categories = cache.GetCategoriesByMainCategory((CategoryEnums)id);

                if (categories.IsNullOrEmpty())
                    return NotFound();
                else
                    return View("CategoryForm", categories);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost]
        public IActionResult TryGetTreeView([FromBody] CategoryViewModel viewModel)
        {
            CategoryCache cache = new CategoryCache();
            try
            {
                IList<TreeViewViewModel> treeViewViewModel = cache.GetListChildren(viewModel);
                if (treeViewViewModel.IsNullOrEmpty())
                    return NotFound();
                else
                    return Ok(treeViewViewModel);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost]
        public IActionResult TryGetMenuByCategories()
        {
            CategoryCache cache = new CategoryCache();

            try
            {
                IList<CategoryViewModel> menuOptions = cache.GetAllMenuOptions();
                if (menuOptions == null)
                    return NotFound();
                else
                    return Ok(menuOptions);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public IActionResult TryGetCategoriesFathersAndChildren([FromBody] CategoryViewModel viewModel)
        {
            CategoryCache cache = new CategoryCache();

            try
            {
                CategoryViewModel categories = cache.GetCategoriesFathersAndChildren(viewModel);

                if (categories == null)
                    return NotFound();
                else
                    return Ok(categories);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public IActionResult TrySaveChanges([FromBody] CategoryViewModel ViewModel)
        {
            CategoryCache cache = new CategoryCache();

            try
            {
                CategoryViewModel Category = cache.TryEdit(ViewModel);
                return Created("", Category);
            }
            catch (Exception ex)
            {
                string Message = $"Error al insertar un nuevo registro: error -> {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost, ActionName("TryDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult TryDelete(int ID_Record)
        {
            CategoryCache cache = new CategoryCache();

            try
            {
                cache.Delete(ID_Record);
                return Json(ID_Record.ToString());
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult GetCategories()
        {
            CategoryCache cache = new CategoryCache();

            try
            {
                CategoryViewModel categories = cache.GetCategories();

                if (categories.IsNullOrEmpty())
                    return NotFound();
                else
                    return Ok(categories);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        IActionResult GetCategoriesByMainCategory(CategoryEnums enums)
        {
            CategoryCache cache = new CategoryCache();

            try
            {
                CategoryViewModel categories = cache.GetCategoriesByMainCategory(enums);

                if (categories.IsNullOrEmpty())
                    return NotFound();
                else
                    return Ok(categories);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost]
        public IActionResult GetAllEntities()
        {
            CategoryCache cache = new CategoryCache();

            try {
                IList<CategoryViewModel> categories = cache.GetAllCategories();

            if (categories.IsNullOrEmpty())
                return NotFound();
            else
                return Ok(categories);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }

        [HttpPost]
        public IActionResult GetCountries() => GetCategoriesByMainCategory(CategoryEnums.Pais);

        [HttpPost]
        public IActionResult GetRoles() => GetCategoriesByMainCategory(CategoryEnums.Roles);

        [HttpPost]
        public IActionResult GetGenders() => GetCategoriesByMainCategory(CategoryEnums.Generos);

        [HttpPost]
        public IActionResult GetTypeDocumentIdentification() => GetCategoriesByMainCategory(CategoryEnums.DocumentosIdentificación);

        [HttpPost]
        public IActionResult GetStatesInterview() => GetCategoriesByMainCategory(CategoryEnums.EstatusEntrevistas);

        [HttpPost]
        public IActionResult GetTopicsInteview() => GetCategoriesByMainCategory(CategoryEnums.Topicos);

        [HttpPost]
        public IActionResult GetSubjects() => GetCategoriesByMainCategory(CategoryEnums.Materias);
    }
}