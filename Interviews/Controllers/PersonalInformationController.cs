using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interviews.Models.Caches;
using Interviews.Models.ViewModels;
using Interviews.Models.Wizards;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interviews.Controllers
{
    public class PersonalInformationController : Controller
    {
        public ILogger ILogger { get; set; }
        public PersonalInformationController(ILogger<PersonalInformationController> logger)
        {
            ILogger = logger;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Informacion personal";
            ViewBag.TextButton = " Agregar nueva información";
            ViewBag.FontAwesomeIcon = "far fa-address-book";
            ViewBag.Controller = "PersonalInformation";
            ViewBag.Parameter = "id_PersonalInformation";
            return View();
        }

        [HttpPost]
        public IActionResult TryPopulateDataTable([FromBody]int id)
        {
            PersonalInformationCache personInfCache = new PersonalInformationCache();
            try
            {
                PersonalInformationCache dataTable = personInfCache.GetDataTableInformationByPerson(new PersonViewModel { Id_Person = Convert.ToString(id) });
                return Ok(dataTable);
            }
            catch (Exception ex)
            {
                string Message = $"Error al obtener los registros: {ex}";
                ILogger.LogError(Message);
                return BadRequest(Message);
            }
        }
    }
}