﻿using Interviews.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interviews.Models.Repositories {
		public interface ISubjectByProfessorRepository : IRepository<SubjectByProfessor> { 
			IList<SubjectByProfessor> SearchWithSubjectsAndProfessors (Func<SubjectByProfessor, bool> predicate);
		}
		public class SubjectByProfessorRepository : Repository<SubjectByProfessor>, ISubjectByProfessorRepository { 
			public IList<SubjectByProfessor> SearchWithSubjectsAndProfessors (Func<SubjectByProfessor, bool> predicate) =>
				_EF.SubjectsByProfessors
				.Include(x => x.Professor)
				.ThenInclude(x => x.Person)
				.Include(x => x.Subject)
                .Where(predicate)
				.ToList();
		}
}
